import { Component, OnInit } from '@angular/core';
import {Store} from '@ngrx/store';
import {AppState} from '../state/app-state';
import {allAction,updateTitle,addMember,updateDesc,updateDuedate,deleteMember,deleteProject,paginate} from '../Action/action';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { faPen,faPlus,faMinusCircle } from '@fortawesome/free-solid-svg-icons';
import {DeleteProjectService} from '../service/delete-project.service';
import {SearchMemberService} from '../service/search-member.service';
import { trigger, transition, useAnimation } from '@angular/animations';


@Component({
  selector: 'app-quick-view-modal',
  templateUrl: './quick-view-modal.component.html',
  styleUrls: ['./quick-view-modal.component.css']
})
export class QuickViewModalComponent implements OnInit {

  faTimes = faTimes;
  faPen = faPen;
  faPlus = faPlus;
  faMinusCircle = faMinusCircle;

  constructor() { }

  ngOnInit() {
  }

}
