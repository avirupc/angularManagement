import { Component, OnInit } from '@angular/core';
import {RouterParamsCatcherService} from '../service/router-params-catcher.service';
import {SearchMemberService} from '../service/search-member.service';
import {UserList} from '../interface/user-list';

@Component({
  selector: 'app-searched-user-list',
  templateUrl: './searched-user-list.component.html',
  styleUrls: ['./searched-user-list.component.css']
})
export class SearchedUserListComponent implements OnInit {

  username:string;	
  private user:UserList;
  private userArr:UserList[];

  constructor(private searchmemebr:SearchMemberService,private paramCatch:RouterParamsCatcherService) { }

  ngOnInit() {

  	this.username = this.paramCatch.get_id();
  	this.searchmemebr.getUserList(JSON.stringify({userName:this.username}))
  	.subscribe(
  		data=>{
  			console.log(data);
  			if(data.hasOwnProperty('status')){
  				if(data['status'] == 'Success'){
  					// data.
  				}
  			}
  		},
  		err=>{
  			console.log(err)
  		}
  	)

  }

}
