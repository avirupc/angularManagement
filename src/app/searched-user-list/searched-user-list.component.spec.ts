import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchedUserListComponent } from './searched-user-list.component';

describe('SearchedUserListComponent', () => {
  let component: SearchedUserListComponent;
  let fixture: ComponentFixture<SearchedUserListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchedUserListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchedUserListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
