import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule,Routes } from '@angular/router';
import{AddProjectComponent} from '../user-dashboard/add-project/add-project.component';
import {HomeComponent} from '../home/home.component';
import {NotFoundComponent} from '../not-found/not-found.component';
import {ProjectViewComponent} from '../project/project-view/project-view.component';
import {SearchedUserListComponent} from '../searched-user-list/searched-user-list.component';
import {UsersComponent} from '../users/users.component';
import {ProjectModule} from '../project/project.module';
import {AddTaskComponent} from '../project/add-task/add-task.component';
import {TaskViewComponent} from '../task/task-view/task-view.component';

// USER CHILDREN
import{DashboardComponent} from '../user-dashboard/dashboard/dashboard.component';
import{UserProjectComponent} from '../user-dashboard/user-project/user-project.component';
import{UserTaskComponent} from '../user-dashboard/user-task/user-task.component';
import{UserTeamComponent} from '../user-dashboard/user-team/user-team.component';

// IMPORT RESOLVER
import {UserResolverService} from '../service/user-resolver.service';
import {DashboardChildResolverService} from '../service/dashboard-child-resolver.service';

// IMPORT AUTHGURDS SERVICE

import {AuthGuardService} from '../authGurds/auth-guard.service';

// IMPORTPROJECT GURDS SERVICE

import {ProjectGurdsService} from '../projectGurds/project-gurds.service';

// IMPORT ALL COMPONENT FOR ROUTING

import { UserDashboardComponent } from '../user-dashboard/user-dashboard/user-dashboard.component';
import {RegistrationComponent} from '../registration/registration/registration.component';
import{LoginComponent} from '../registration/login/login.component';
import {ForgottpasswordComponent} from '../registration/forgottpassword/forgottpassword.component';

import {ProjectQuickViewComponent} from '../user-dashboard/user-project/project-quick-view/project-quick-view.component';
import {ProjectDetailsComponent} from '../user-dashboard/project-details/project-details.component';
import { ProjectInfoComponent } from '../user-dashboard/project-details/project-info/project-info.component';
import { TaskBoardComponent } from '../user-dashboard/project-details/task-board/task-board.component';
import {ClosedProjectsComponent} from '../user-dashboard/closed-projects/closed-projects.component';
import {OpenProjectsComponent} from '../user-dashboard/open-projects/open-projects.component';
import {TrashedProjectComponent} from '../user-dashboard/trashed-project/trashed-project.component';
import {ImageUploadComponent} from '../image-upload/image-upload.component';
import {CommentBoxComponent} from '../user-dashboard/project-details/comment-box/comment-box.component';
import {SearchResultComponent} from '../user-dashboard/search-result/search-result.component';
import {WorkspaceComponent} from '../workspace/workspace.component';
import {WorkspaceChildComponent} from '../workspace/workspace-child/workspace-child.component';

const routes:Routes = [
{path:'', pathMatch:'full', redirectTo:'home'},
{path:'home',component:HomeComponent,children:[
{path:'search/:username',component:SearchedUserListComponent}
]},
{path:'dashboard/:userid',component:UserDashboardComponent,canActivate:[AuthGuardService],resolve:{'userDetails':UserResolverService},
children:[
	{path:'', pathMatch:'full', redirectTo:'dashboard',resolve:{'userDetails':UserResolverService}},	
	{path:'dashboard',component:DashboardComponent,resolve:{'userDetails':UserResolverService}},
	{path:'workspace',component:WorkspaceComponent},
	{path:'workspace/:id',component:WorkspaceChildComponent},
	{path:'project/:id',component:ProjectDetailsComponent,children:[
		{path:'',pathMatch:'full', redirectTo:'info'},	    
		{path:'info',component:ProjectInfoComponent},
		{path:'taskboard',component:TaskBoardComponent},
		{path:'comment',component:CommentBoxComponent}
	]},	
	{path:'user-project',component:UserProjectComponent,resolve:{'userDetails':UserResolverService},
	children:[{path:'quickView/:id' ,component:ProjectQuickViewComponent,data:{state:'ProjectQuickView'}},
		{path:'', pathMatch:'full', redirectTo:'openProjects'},	
		{path:'search',component:SearchResultComponent,children:[
			{path:'quickView/:id',component:ProjectQuickViewComponent,data:{state:'ProjectQuickView'}}
		]},
		{path:'addProject',component:AddProjectComponent,canActivate:[AuthGuardService]},
		{path:'closed-project',component:ClosedProjectsComponent,children:[
		{path:'quickView/:id' ,component:ProjectQuickViewComponent,data:{state:'ProjectQuickView'}}
		]},
		{path:'openProjects',component:OpenProjectsComponent,children:[
		{path:'quickView/:id' ,component:ProjectQuickViewComponent,data:{state:'ProjectQuickView'}}
		]},
		{path:'trashedProjects',component:TrashedProjectComponent,children:[
		{path:'quickView/:id' ,component:ProjectQuickViewComponent,data:{state:'ProjectQuickView'}}
]
},
	{path:'user-task',component:UserTaskComponent},
	{path:'user-team',component:UserTeamComponent}	
]}
]},
{path:'user/:userId',component:UsersComponent},
{path:'registration',component:RegistrationComponent},
{path:'login',component:LoginComponent},
{path:'project/:projectid',component:ProjectViewComponent,canActivate:[ProjectGurdsService],children:[
	{path:'addTask',component:AddTaskComponent},
	{path:'task/:id',component:TaskViewComponent}
]},
{path:'login_help',component:ForgottpasswordComponent},
{path:'user-image',component:ImageUploadComponent},
{path:'**',component:NotFoundComponent}
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes,{onSameUrlNavigation: 'reload'}),
    ProjectModule
  ],
  exports:[RouterModule] 
})
export class AppRouterModule { }
