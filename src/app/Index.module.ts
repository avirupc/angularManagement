import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BrowserModule} from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import {ReactiveFormsModule} from '@angular/forms';
import{FontAwesomeModule} from '@fortawesome/angular-fontawesome';
// IMPORT STORE MODULE
import {StoreModule} from '@ngrx/store';
// IMPORT REDUCER
import {appReducer} from './reducer/app-reducer';

import {RegistrationModule} from './registration/registration.module';
import {UserDashboardModule} from './user-dashboard/user-dashboard.module';
import {CommonViewModule} from './common-view/common-view.module';
// import {ProjectModule} from './project/project.module';
import {ProjectModule} from './project/project.module';
import {TaskModule} from './task/task.module';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FileSelectDirective } from 'ng2-file-upload';

// IMPORT ROUTER MODULE
import {AppRouterModule} from './app-router/app-router.module';


// IMPORT SERVICE

import{UserDataProviderService} from './service/user-data-provider.service';
import {GetUserDetailsService} from './service/get-user-details.service';
import { getProjectService } from './service/get-project.service';
import {AuthGuardService} from './authGurds/auth-guard.service';
import {UserResolverService} from './service/user-resolver.service';
import {DashboardChildResolverService} from './service/dashboard-child-resolver.service';
import {HttpinterceptorsService} from './service/httpinterceptors.service';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {LoaderService} from './service/loader.service';
import {SharedLogoutService} from './service/shared-logout.service';

// IMPORT BOOTSTRAP COMPONENT
import {IndexComponent} from './index/index.component';

import { HomeComponent } from './home/home.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { SearchedUserListComponent } from './searched-user-list/searched-user-list.component';
import { UsersComponent } from './users/users.component';
import { TabComponent } from './tab-component/tab-component.component';
import { LoaderComponent } from './loader/loader.component';

// SOCIAL LOGIN

import {
    SocialLoginModule,
    AuthServiceConfig,
    GoogleLoginProvider,    
    FacebookLoginProvider,
} from "angular-6-social-login";
import { NotificationComponent } from './notification/notification.component';
import { QuickViewModalComponent } from './quick-view-modal/quick-view-modal.component';
import { ImageUploadComponent } from './image-upload/image-upload.component';
import { SafeHtmlPipe } from './pipe/safe-html.pipe';
import { WorkspaceComponent } from './workspace/workspace.component';
import { WorkspaceChildComponent } from './workspace/workspace-child/workspace-child.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { ModalComponent } from './modal/modal.component';



 
export function getAuthServiceConfigs() {
const config = new AuthServiceConfig(
[
{
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider('Your_Facebook_Client_ID')
},
{
    id: GoogleLoginProvider.PROVIDER_ID,
    provider:new GoogleLoginProvider("294653247959-1uf85g2lg48lsm8dlbhql34ih9faos1s.apps.googleusercontent.com")
}
]
);
return config;
}



@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    AppRouterModule,
    RegistrationModule,
    UserDashboardModule,
    CommonViewModule,
    FormsModule,
    ProjectModule,
    ReactiveFormsModule,
    TaskModule,
    StoreModule.forRoot({'mainState':appReducer}),
    SocialLoginModule,
    BrowserAnimationsModule,
    FontAwesomeModule
  ],
  declarations: [IndexComponent, HomeComponent, NotFoundComponent, SearchedUserListComponent, UsersComponent, TabComponent, LoaderComponent, NotificationComponent, QuickViewModalComponent, ImageUploadComponent, SafeHtmlPipe, WorkspaceComponent, WorkspaceChildComponent, UserProfileComponent, ModalComponent],
  providers:[AuthGuardService,UserDataProviderService,GetUserDetailsService,getProjectService,UserResolverService,DashboardChildResolverService,{provide:HTTP_INTERCEPTORS,useClass:HttpinterceptorsService,multi:true},SharedLogoutService,{provide:AuthServiceConfig,useFactory:getAuthServiceConfigs}],
  bootstrap:[IndexComponent],
  exports:[SafeHtmlPipe]  
})
export class IndexModule {
}
