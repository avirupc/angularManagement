import { Injectable } from '@angular/core';
import{User} from '../class/user';
import {Observable,of} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RouterParamsCatcherService {

  public id:any;
  public userId:any;
  public user:User;	
  public u:Observable<User>;

  constructor() { }

  set_id(id:any){
  	this.id = id;
  }

  get_id(){
  	return this.id;
  }

  set_user_id(id){
  	this.userId = id;
  }

  get_user_id(){
  	return this.userId;
  }

  set_user(user){
  	this.user = user;
  	this.u = new Observable(o =>{
  		o.next(this.user);
  		o.complete()
  	})  	
  }
  get_user(){
  	return this.u;
  }

}
