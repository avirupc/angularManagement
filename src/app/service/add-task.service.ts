import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AddTaskService {

private  url:string = 'http://quicktask.in/avirup/admin/user/addTask';
  constructor(private http:HttpClient) { }


  public addTask(info){
  	return this.http.post(this.url,info);
  }
}
