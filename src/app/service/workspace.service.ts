import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class WorkspaceService {

	addWorkspaceUrl:string = 'http://quicktask.in/avirup/admin/user/addWorkSpace';
	getWorkspaceUrl:string = 'http://quicktask.in/avirup/admin/user/getWorkspace';
	deleteWorkspaceUrl:string = 'http://quicktask.in/avirup/admin/user/deleteWokspace'
  constructor(private http:HttpClient) { }

  addWorkspace(info:any){  	
  	console.log(info);
  	return this.http.post(this.addWorkspaceUrl,info);
  }

  getWorkspace(userId:any){
  	return this.http.post(this.getWorkspaceUrl,userId);
  }

  deleteWorkspace(ws_id:any){
  	return this.http.post(this.deleteWorkspaceUrl,ws_id)
  }
}
