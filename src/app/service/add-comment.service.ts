import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AddCommentService {

	private projectCommentUrl :string = 'http://www.quicktask.in/avirup/admin/user/addComment'
	private getCommentUrl :string = 'http://www.quicktask.in/avirup/admin/user/getComment'

  constructor(private http:HttpClient) { }

  addComment(info:any){
  	return this.http.post(this.projectCommentUrl,info);
  }
   getComment(info:any){
  	return this.http.post(this.getCommentUrl,info);
  }
}
