import { Injectable } from '@angular/core';
import {Resolve,ActivatedRoute,RouterState,Router} from '@angular/router';
import {User} from '../class/user';
import{Observable} from 'rxjs';
import {RouterParamsCatcherService} from './router-params-catcher.service';
import {UserDashboardComponent} from '../user-dashboard/user-dashboard/user-dashboard.component';

@Injectable({
  providedIn: 'root'
})
export class DashboardChildResolverService implements Resolve<any> {

  constructor(private activateUser:RouterParamsCatcherService,private active:ActivatedRoute) {

  	active.params.subscribe(data =>{
  		console.log(data)
  	})

   }

  resolve():Observable<any>{ 	
  	// console.log(this.active.snapshot)
  	return this.activateUser.get_user()
  }
}
