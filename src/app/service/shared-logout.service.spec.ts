import { TestBed } from '@angular/core/testing';

import { SharedLogoutService } from './shared-logout.service';

describe('SharedLogoutService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SharedLogoutService = TestBed.get(SharedLogoutService);
    expect(service).toBeTruthy();
  });
});
