import { TestBed } from '@angular/core/testing';

import { DeleteProjectService } from './delete-project.service';

describe('DeleteProjectService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DeleteProjectService = TestBed.get(DeleteProjectService);
    expect(service).toBeTruthy();
  });
});
