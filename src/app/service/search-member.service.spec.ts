import { TestBed, inject } from '@angular/core/testing';

import { SearchMemberService } from './search-member.service';

describe('SearchMemberService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SearchMemberService]
    });
  });

  it('should be created', inject([SearchMemberService], (service: SearchMemberService) => {
    expect(service).toBeTruthy();
  }));
});
