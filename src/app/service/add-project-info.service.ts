import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AddProjectInfoService {

	titleUrl:string = 'http://quicktask.in/avirup/admin/project/updateProjectTitle';
	addMemberUrl:string = 'http://quicktask.in/avirup/admin/project/addProjectMember';
	addAdminUrl:string = 'http://quicktask.in/avirup/admin/project/addProjectAdmin';
	deleteAdminUrl:string = 'http://quicktask.in/avirup/admin/project/deleteProjectAdmin';
	addDesc:string = 'http://quicktask.in/avirup/admin/project/addDescription';
	addDuedate:string = 'http://quicktask.in/avirup/admin/project/addDueDate';
	deleteMemberurl:string = 'http://quicktask.in/avirup/admin/project/deleteProjectMember';
	closeProjectUrl:string = 'http://quicktask.in/avirup/admin/user/closeProject';
	openProjectUrl:string = 'http://quicktask.in/avirup/admin/user/openProject'

  constructor(private http:HttpClient) { }

  public updateTitle(titleinfo:any){
  	return this.http.post(this.titleUrl,titleinfo);
  }

  public updateDesc(description:any){
  	return this.http.post(this.addDesc,description);
  }

  public updateDuedate(duedate:any){  	
  	return this.http.post(this.addDuedate,duedate);
  }

  public deleteMember(member:any){
  	alert()
  	return this.http.post(this.deleteMemberurl,member);
  }

  public addMember(projectId:any,userId:any){
  	let memberInfo = {'projectId':projectId,'userId':userId};
  	console.log(memberInfo);
  	return this.http.post(this.addMemberUrl,memberInfo);
  }

  public closeProject(id:any){
  	return this.http.post(this.closeProjectUrl,id);
  }
   public reOpenProject(id:any){
  	return this.http.post(this.openProjectUrl,id);
  }

  public addAdmin(info:any){
  	return this.http.post(this.addAdminUrl,info);
  }

    public deleteAdmin(info:any){
  	return this.http.post(this.deleteAdminUrl,info);
  }
}
