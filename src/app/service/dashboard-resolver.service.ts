import { Injectable } from '@angular/core';
import {Resolve,ActivatedRoute,RouterState} from '@angular/router';
import {User} from '../class/user';
import{Observable} from 'rxjs';
import {RouterParamsCatcherService} from './router-params-catcher.service';

@Injectable({
  providedIn: 'root'
})
export class DashboardResolverService implements Resolve<any> {

  constructor(private userCatcher:RouterParamsCatcherService) { }

  resolve():Observable<any>{
  	
  	return this.userCatcher.get_user();
  }
}
