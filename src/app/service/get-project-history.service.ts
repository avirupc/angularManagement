import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class GetProjectHistoryService {	

	private url:string='http://www.quicktask.in/avirup/admin/project/getProjectHistory';

  constructor(private http:HttpClient) { }

  getHistory(info:any){
  	return this.http.post(this.url,info);
  }
}
