import { TestBed } from '@angular/core/testing';

import { GetProjectHistoryService } from './get-project-history.service';

describe('GetProjectHistoryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GetProjectHistoryService = TestBed.get(GetProjectHistoryService);
    expect(service).toBeTruthy();
  });
});
