import { Injectable }  from '@angular/core';
import { HttpClient  } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class SearchMemberService {

	url:string = 'http://quicktask.in/avirup/admin/user/getUserList';

  	constructor(private http:HttpClient) { }

  	public getUserList(username:any){
  		console.log(username)
  		return this.http.post(this.url,username);
  	}




}
