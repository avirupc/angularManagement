import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';

declare const Pusher;

@Injectable({
  providedIn: 'root'
})
export class PusherService {
	pusher:any;
  constructor() {
  	this.pusher = new Pusher(environment.pusher.key);
   }
}
