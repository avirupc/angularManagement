import { TestBed, inject } from '@angular/core/testing';

import { ProjectIdCatcherService } from './project-id-catcher.service';

describe('ProjectIdCatcherService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProjectIdCatcherService]
    });
  });

  it('should be created', inject([ProjectIdCatcherService], (service: ProjectIdCatcherService) => {
    expect(service).toBeTruthy();
  }));
});
