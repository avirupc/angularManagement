import { Injectable } from '@angular/core';
import {Observable,BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataObserverService {

public sub = new BehaviorSubject<any>('');
public ob = this.sub.asObservable();

 public o = Observable.create((observer:any)=>{
  	try{
  		observer.next('Hi');
  		observer.next('Hello');
  		observer.next('ss');
  	}catch (err){
  		observer.error(err);
  	}
  });

  constructor() {
  	let s = this.sub;
  	setTimeout(function(){s.next('Hello');},3000)
  
  // this.sub.next('data');
}
}
