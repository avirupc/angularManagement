import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class DeleteProjectService {

	deleteUrl :string = 'http://quicktask.in/avirup/admin/user/deleteProject';
	restoreUrl :string = 'http://quicktask.in/avirup/admin/user/restoreProject';
	permanentDeleteUrl :string = 'http://quicktask.in/avirup/admin/user/permanentDeleteProject';

  constructor(private http:HttpClient) { }

  deleteProject(id:any){  	
  	return this.http.post(this.deleteUrl,{'project_id':id})
  }
  restoreProject(id:any){
  	return this.http.post(this.restoreUrl,{'project_id':id})
  }

  permanentDelete(id:any){
  	return this.http.post(this.permanentDeleteUrl,{'project_id':id})
  }
}
