import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AddProjectService {

	url:string = 'http://quicktask.in/avirup/admin/user/addProject';

  constructor(private http:HttpClient) { }

  public projectAddition(info){

  	return this.http.post(this.url,info)

  }

}
