import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';


@Injectable({
  providedIn: 'root'
})
export class SharedLogoutService {

	 // Observable string sources
	private Logout = new Subject<any>();
	// Observable string streams
	public logoutObservable = this.Logout.asObservable();

	// Service message comand

	public logoutCommand(change:any){
		this.Logout.next(change);
	}

  constructor() { }
}
