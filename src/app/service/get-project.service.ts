import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class getProjectService {

	// PUBLIC PROJECT URL

	publicProjectUrl:string = 'http://quicktask.in/avirup/admin/project/getPublicProject';
	projectDetailsUrl:string = 'http://quicktask.in/avirup/admin/project/getProjectDetails';
	userIsMemberurl:string = 'http://quicktask.in/avirup/admin/project/isMember';

  constructor(private http:HttpClient) { }


  public getAllPublicProject(){
  	return this.http.get(this.publicProjectUrl);
  }

  public getProjectDetails(id:any){
  	return this.http.post(this.projectDetailsUrl,{'projectId':id});
  }

  public logedInUserIsMember(data:any){
  	return this.http.post(this.userIsMemberurl,data);
  }
}
