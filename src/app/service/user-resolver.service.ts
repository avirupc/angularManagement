import { Injectable,OnInit } from '@angular/core';
import {Resolve,ActivatedRoute,RouterState} from '@angular/router';
import {User} from '../class/user';
import{Observable} from 'rxjs';
import {GetUserDetailsService} from './get-user-details.service';

@Injectable({
  providedIn: 'root'
})
export class UserResolverService implements Resolve<any>,OnInit {

	public userDetail:any;

	ngOnInit(){
		
	}
	
	userID:any;	
  constructor(private userDetails:GetUserDetailsService,private route: ActivatedRoute) {

  	
	
   }

  resolve():Observable<any>{

  	this.userID = localStorage.getItem('id');  	
  	return this.userDetails.getUserData({userId:this.userID})	
  	
  }
}
