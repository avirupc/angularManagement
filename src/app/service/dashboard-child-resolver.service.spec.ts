import { TestBed, inject } from '@angular/core/testing';

import { DashboardChildResolverService } from './dashboard-child-resolver.service';

describe('DashboardChildResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DashboardChildResolverService]
    });
  });

  it('should be created', inject([DashboardChildResolverService], (service: DashboardChildResolverService) => {
    expect(service).toBeTruthy();
  }));
});
