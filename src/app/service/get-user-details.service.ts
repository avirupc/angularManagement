import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable,BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GetUserDetailsService {

   sub = new BehaviorSubject<any>({});	
   ob = this.sub.asObservable();

 url:string = 'http://quicktask.in/avirup/admin/user/showUserDetails';	
  constructor(private http:HttpClient) {
  
   }



   getUserData(userInfo){   	
   return this.http.post(this.url,userInfo)
   }
}
