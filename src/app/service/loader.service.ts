import { Injectable } from '@angular/core';
import {Subject} from 'rxjs';
import {Loaderstate} from '../interface/loaderstate';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {

 private loaderSubject = new Subject<Loaderstate>();
 loaderState = this.loaderSubject.asObservable();

  constructor() { }

  show(){
  	this.loaderSubject.next(<Loaderstate>{show:true});
  }

  hide() {
    this.loaderSubject.next(<Loaderstate>{show:false});
  }
}
