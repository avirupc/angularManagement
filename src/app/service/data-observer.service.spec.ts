import { TestBed } from '@angular/core/testing';

import { DataObserverService } from './data-observer.service';

describe('DataObserverService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DataObserverService = TestBed.get(DataObserverService);
    expect(service).toBeTruthy();
  });
});
