import { TestBed } from '@angular/core/testing';

import { AddProjectInfoService } from './add-project-info.service';

describe('AddProjectInfoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AddProjectInfoService = TestBed.get(AddProjectInfoService);
    expect(service).toBeTruthy();
  });
});
