import { Injectable } from '@angular/core';
import {HttpInterceptor,HttpRequest,HttpHandler,HttpEvent,HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {LoaderService} from './loader.service';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HttpinterceptorsService implements HttpInterceptor {



	intercept(req:HttpRequest<any>,next:HttpHandler):Observable<HttpEvent<any>>{

		this.showLoader();		
		// next.handle(req).subscribe(data =>{
		// 	if(data instanceof HttpResponse){
		// 		this.hideloader();
		// 	}
		// },
		// err=>{
		// 		this.hideloader()	
		// 	}

		// )
		return next.handle(req).pipe(tap((event: HttpEvent<any>) => { 
		      if (event instanceof HttpResponse) {
		        this.hideloader();
		      }
		    },
		      (err: any) => {
		        this.hideloader();
		    }));		  
	}

  constructor(private loader:LoaderService) { }

  private showLoader(){
  	this.loader.show()
  }
  private hideloader(){
  	this.loader.hide()
  }
}
