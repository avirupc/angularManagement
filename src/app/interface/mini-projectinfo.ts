export interface MiniProjectinfo {

	id:any;
	name:string;
	description:string;
	is_creator:boolean;
	is_admin:boolean
}
