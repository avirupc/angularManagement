import{Action} from '@ngrx/store';

export class appAction implements Action {

readonly type:string;
constructor(public payload:any){}

}

export class Login implements Action {
	
	readonly type:string = 'LOGEDIN';
	constructor(public payload:any){}

}

export class Logout implements Action {
	
    readonly type:string = 'LOGOUT';
	constructor(public payload:any) {
	
	}
}

export class getUserDetails implements Action {

	type:string = 'GETUSER'
	constructor(public payload:any){}

}

export class updateTitle implements Action {

	type:string = 'UPDATE_TITLE'
	constructor(public payload:any){}

}

export class addMember implements Action {

	type:string = 'ADD_MEMBER'
	constructor(public payload:any){}

}
export class addAdmin implements Action {

	type:string = 'ADD_ADMIN'
	constructor(public payload:any){}

}
export class deleteAdmin implements Action {

	type:string = 'DELETE_ADMIN'
	constructor(public payload:any){}

}
export class updateDesc implements Action {

	type:string = 'UPDATE_DESCRIPTION'
	constructor(public payload:any){}
}

export class updateDuedate implements Action {

	type:string = 'UPDATE_DUEDATE'
	constructor(public payload:any){}

}
export class deleteMember implements Action {

	type:string = 'DELETE_MEMBER'
	constructor(public payload:any){}
}

export class addProject implements Action {
	type:string = 'ADD_PROJECT';
	constructor(public payload:any){}
}
export class deleteProject implements Action {
	type:string = 'DELETE_PROJECT';
	constructor(public payload:any){}
}

export class permanentDeleteProject implements Action {
	type:string = 'PERMANENT_DELETE_PROJECT';
	constructor(public payload:any){}
}

export class sliceProjects implements Action {
	type:string = 'SLICE_PROJECTS';
	constructor(public payload:any){}
}

export class paginate implements Action {
	type:string = 'PAGINATE';
	constructor(public payload:any){}
}
export class closedProjects implements Action {
	type:string = 'CLOSED_PROJECTS';
	constructor(public payload:any){}
}

export class closeProject implements Action {
	type:string = 'CLOSE_PROJECT';
	constructor(public payload:any){}
}
export class getTrashedProjects implements Action {
	type:string = 'GET_TRASHED_PROJECTS';
	constructor(public payload:any){}
}
export class reOpenProject implements Action {
	type:string = 'REOPEN_PROJECT';
	constructor(public payload:any){}
}
export class reStoreProject implements Action {
	type:string = 'RESTORE_PROJECT';
	constructor(public payload:any){}
}

export class getAllProjects implements Action {
	type:string = 'All_PROJECTS';
	constructor(public payload:any){}
}
export class getProject implements Action {
	type:string = 'GET_PROJECT';
	constructor(public payload:any){}
}

export class getComment implements Action {
	type:string = 'GET_COMMENT';
	constructor(public payload:any){}
}
export class addComment implements Action {
	type:string = 'ADD_COMMENT';
	constructor(public payload:any){}
}
export class searchProject implements Action {
	type:string = 'SEARCH_PROJECS';
	constructor(public payload:any){}
}

export class searchProjectByName implements Action {
	type:string = 'SEARCH_PROJECT_BY_NAME';
	constructor(public payload:any){}
}

export class lastSearchResult implements Action {
	type:string = 'LAST_SEARCH_RESULT';
	constructor(public payload:any){}
}

export class getWorkspace implements Action {
	type:string = 'GET_WORKSPACE';
	constructor(public payload:any){}
}
export class modalOpen implements Action {
	type:string = 'MODAL_OPEN';
	constructor(public payload:any){}
}
export class modalClose implements Action {
	type:string = 'MODAL_CLOSE';
	constructor(public payload:any){}
}
export class workspaceModal implements Action {
	type:string = 'WORKSPACE_MODAL';
	constructor(public payload:any){}
}
export class addWorkspace implements Action {
	type:string = 'WORKSPACE_ADD';
	constructor(public payload:any){}
}
export class deleteWorkspace implements Action {
	type:string = 'WORKSPACE_DELETE';
	constructor(public payload:any){}
}


export type allAction = appAction | Login |Logout|getUserDetails |updateTitle|addMember |updateDesc|updateDuedate|deleteMember|addProject|deleteProject|sliceProjects|paginate|closedProjects|getAllProjects|closeProject|reOpenProject|getProject|getTrashedProjects|reStoreProject|permanentDeleteProject|getComment|addComment|addAdmin|deleteAdmin|searchProject|searchProjectByName|lastSearchResult|getWorkspace|modalOpen|modalClose|workspaceModal|addWorkspace|deleteWorkspace;
