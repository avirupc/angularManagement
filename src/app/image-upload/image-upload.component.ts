import { Component, OnInit } from '@angular/core';
import {  FileUploader, FileSelectDirective } from 'ng2-file-upload/ng2-file-upload';

@Component({
  selector: 'app-image-upload',
  templateUrl: './image-upload.component.html',
  styleUrls: ['./image-upload.component.css']
})
export class ImageUploadComponent implements OnInit {

	private url:string = 'http://quicktask.in/avirup/admin/user/imageupload';
	public uploader: FileUploader = new FileUploader({url:this.url,itemAlias:'photo'})

  constructor() { }

  ngOnInit() {

  	this.uploader.onBuildItemForm  = (file:any,form:any) =>{
  		form.append('user_id',304);
  	}

  	this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false; };
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
         console.log(response);
         let data  = JSON.parse(response);
         if(data.hasOwnProperty('status') && data['status'].toLowerCase() == 'success'){
         	alert('File uploaded successfully');
         }else if(data.hasOwnProperty('status') && data['status'].toLowerCase() == 'failure'){
         	alert(data['message'])
         }

         
     };  
  }


}
