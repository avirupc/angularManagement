import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkspaceChildComponent } from './workspace-child.component';

describe('WorkspaceChildComponent', () => {
  let component: WorkspaceChildComponent;
  let fixture: ComponentFixture<WorkspaceChildComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkspaceChildComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkspaceChildComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
