import { Component, OnInit } from '@angular/core';
import {Store} from '@ngrx/store';
import {AppState} from '../../state/app-state';
import {allAction,getWorkspace,modalOpen} from '../../Action/action';
import {Router,ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-workspace-child',
  templateUrl: './workspace-child.component.html',
  styleUrls: ['./workspace-child.component.css']
})
export class WorkspaceChildComponent implements OnInit {

	private state:any;
	private ws_id:any;
	private workspace:any;



  constructor(private store:Store<AppState>,private activeRoute:ActivatedRoute,private router: Router) { }

  ngOnInit() {

  	// force route reload whenever params change;
      // this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  	
  	this.state = this.store.select('mainState');
  	this.activeRoute.params.subscribe((ws_id)=>{

  		if(ws_id.hasOwnProperty('id')){ 
  			this.ws_id = ws_id.id;  			
  			// this.store.dispatch(new getWorkspace({'id':this.ws_id}))        
  		}  
  	})
  	this.state.subscribe((data)=>{
  		this.workspace = data.currentWorkspace;
  	})
  }

  addProject(){
    alert()
    this.store.dispatch(new modalOpen({}));
  }

}
