import { Component, OnInit } from '@angular/core';
import {WorkspaceService} from '../service/workspace.service';
import {Store} from '@ngrx/store';
import {AppState} from '../state/app-state';
import {allAction,getWorkspace,modalOpen,workspaceModal,deleteWorkspace} from '../Action/action';
import {Router,ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-workspace',
  templateUrl: './workspace.component.html',
  styleUrls: ['./workspace.component.css']
})
export class WorkspaceComponent implements OnInit {

  private state:any;
  constructor(private workspaceService:WorkspaceService,private store:Store<AppState>) { }

  private workspace:any;
  private dropdownShow:boolean;
  private ws_id:any;

  ngOnInit() {
  	this.state = this.store.select('mainState');
  	this.state.subscribe(data =>{
  		console.log(data);
  		this.workspace = data.activeWorkspace;
  	})
  }

  addWorkspace(){ 	

  	this.store.dispatch(new workspaceModal({}));
  	// let workspace = {'ws_title':'demo','ws_desc':'demo','creator_id':304}

  	//  this.workspaceService.addWorkspace(workspace)
  	//  .subscribe(data =>{
  	//  	console.log(data);
  	//  })
  }
  dropDownToggle(wsId:any){
  	if(this.ws_id == wsId){
  		this.dropdownShow = !this.dropdownShow
  	}else{
  		this.dropdownShow = true;
  		this.ws_id = wsId;
  	}
  }

  deleteWorkspace(id:any){
  	let ws = {'ws_id':id}
  	this.workspaceService.deleteWorkspace(ws)
  	.subscribe(data =>{
  		if(data.hasOwnProperty('status') && data['status'].toLowerCase() =='success'){
  			console.log(data)
  			this.store.dispatch(new deleteWorkspace({'ws_id':id}))
  		}
  	})
  }
 
}
