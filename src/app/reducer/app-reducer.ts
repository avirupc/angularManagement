import {Action} from '@ngrx/store';
import {AppState} from '../state/app-state';
import {allAction} from '../Action/action';
import {Workspace} from '../class/workspace';

export class initialState implements AppState{
	isLogin = false;
	user:any = null;
	projects:any = null;
	imutateProjects:any = null;
	closeProjects:any = null;
	paginateProjects:Array<any> = [];
	currentPagination:any=1;
	totalPagination:any;
	paginationArray:Array<any>=[];
	project:any;
	is_complete:boolean;
	comment?:Array<any> = [];
	searchResult:Array<any> = [];
	allworkspace:Array<any>;
	activeWorkspace:Array<any>;
	currentWorkspace:any;	
	modalOpen:boolean = false;
	showModalContent:string='';	
}

export function appReducer(state:initialState,action:allAction):AppState{

	switch (action.type) {
		case 'LOGEDIN':
			return Object.assign({},state,action.payload)
		case 'LOGOUT':
			return Object.assign({},state,action.payload)
		case 'ISADMIN':
			return Object.assign({},state,action.payload)
		case 'ISMEMBER':
		return Object.assign({},state,action.payload)
			case 'GETUSER':
			state.user = action.payload.users[0];
			state.projects = action.payload['projects'];
			// state.allworkspace = action.payload['ws'];
			state.activeWorkspace = action.payload['active_ws']
			state.imutateProjects =action.payload['projects']
		    state.closeProjects =  action.payload['projects'].filter(e=>{
		        return e['is_complete'] == "1";
		      }); 
		      state.comment = [];   					
			return state;
			case 'UPDATE_TITLE':
			state.projects.forEach((e,i)=>{
				if(e['id'] == action.payload['id']){
					e['title'] = action['payload']['title'];
				}
			})			
			return state;
			case 'ADD_MEMBER':
			state.projects.forEach((e,i)=>{
				if(e['id'] == action.payload['projectId']){
					e['total_member'].push({member_id: action['payload']['id'], member_name: action['payload']['name'], user_image: ""});
					e['member_count'] = e['member_count']++;
					// return false;					
				}
			})			
			return state;
			case 'ADD_ADMIN':
			console.log(state)
			state.projects.forEach((e,i)=>{
				if(e['id'] == action.payload['projectId']){
					e['total_admin'].push({'admin_id': action['payload']['id'], 'admin_name': action['payload']['name'], user_image: ""});
					e['admin_count'] = e['admin_count']++;
					// return false;					
				}
			})
			console.log(state)
			return state;
			case 'UPDATE_DESCRIPTION':
			state.projects.forEach((e,i)=>{

				if(e['id'] == action.payload['id']){
					e['description'] = action['payload']['desc'];
				}
			})			
			return state;
					case 'UPDATE_DUEDATE':
			state.projects.forEach((e,i)=>{

				if(e['id'] == action.payload['id']){
					e['dueDate'] = action['payload']['dueDate'];
				}
			})			
			return state;
			case 'DELETE_MEMBER':	
			state.projects.forEach((e,i)=>{
				if(e['id'] == action['payload']['id']){
				for(let i = 0; i < e['total_member'].length; i++) {
			    if(e['total_member'][i]['member_id'] == action['payload']['member_id']) {
			        e['total_member'].splice(i, 1);
			        break;
			    }
				}	
				for(let i = 0; i < e['total_admin'].length; i++) {
			    if(e['total_admin'][i]['admin_id'] == action['payload']['member_id']) {
			        e['total_admin'].splice(i, 1);
			        break;
			    }
				}
				}
			})
			console.log(state)
			return state;
			case 'DELETE_ADMIN':	
			state.projects.forEach((e,i)=>{
				if(e['id'] == action['payload']['id']){
				for(let i = 0; i < e['total_admin'].length; i++) {
			    if(e['total_admin'][i]['admin_id'] == action['payload']['admin_id']) {
			        e['total_admin'].splice(i, 1);
			        break;
			    }
				}
				}
			})
			console.log(state)
			return state;
			case 'ADD_PROJECT':
			let today:any = new Date();
			let dd:any = today.getDate();
			let mm:any = today.getMonth()+1; //January is 0!
			let yyyy:any = today.getFullYear();

			if(dd<10) {
			    dd = '0'+dd
			} 
			if(mm<10) {
			    mm = '0'+mm
			} 
			let todaydate:any = mm + '-' + dd + '-' + yyyy;
			let project = {
				admin_count: 1,
				creator_info: [{creator_id: state['user']['id'], creator_name: state['user']['login_name']}],
				description: action.payload['description'],
				dueDate: action.payload['dueDate'],
				id: action.payload['id'],
				is_admin: "true",
				is_creator: "true",
				is_complete:0,
				is_public: action.payload['is_public'],
				is_trashed:"0",
				member_count: 1,
				startDate:todaydate,
				task_count: 0,
				title: action.payload['title'],
				total_admin: [{member_id: state['user']['id'], member_name:state['user']['login_name'], user_image: ""}],
				total_member:[{member_id: state['user']['id'], member_name: state['user']['login_name'], user_image: ""}]}		
				state.imutateProjects.push(project);				
			return {...state};
			case 'DELETE_PROJECT':		
			state.imutateProjects.forEach((e)=>{
				if(e['id'] == action.payload['projectId']){
					e['is_trashed'] = "1"
				}
			})			
			state.projects = state.projects.filter((e,i)=>{
				return e['id'] !== action.payload['projectId'];	
			})						
			return {...state};
			case 'PERMANENT_DELETE_PROJECT':
			state.imutateProjects = state.imutateProjects.filter(e=>{
				 return e['id'] !== action.payload['projectId']
			})	
			state.projects = state.projects.filter((e,i)=>{
				return e['id'] !== action.payload['projectId'];	
			})						
			return {...state};
			case 'RESTORE_PROJECT':			
			state.imutateProjects.forEach((e)=>{
				if(e['id'] == action.payload['projectId']){
					e['is_trashed'] = "0"
				}
			})			
			state.projects = state.projects.filter((e,i)=>{
				return e['id'] !== action.payload['projectId'];	
			})						
			return {...state};
			case 'PAGINATE':				
			let itemPerpage:any = 5;
			state.paginationArray= [];
			// alert(state.projects.length);
			state.totalPagination = Math.ceil(state.projects.length/itemPerpage);			
  		  	let counter = 0;
		  	for(var x=1 ; x <= state.totalPagination ; x++){		  		
		  		state.paginationArray.push({id:counter,'active':false,'number':x});
		  		counter ++;
		  	}

		  	// FOR ADD ACTIVE CLASS ON ACTIVE PAGINATION

		  	for(var x = 0 ; x < state.paginationArray.length ; x++){
		  			state.paginationArray[x]['active'] = false;
		  		if(x == action.payload['paginationId']){
		  			state.paginationArray[x]['active'] = true;
		  		}
		  	}
		  	// PAGINATION LOGIC

		  state.paginateProjects = [];
		  let arr = [];
		  if(action.payload['paginationNum'] == 1){		  	
		  	state.projects.forEach((e,x)=>{
		  		if(x >=(action.payload['paginationNum']-1)*itemPerpage && x < action.payload['paginationNum']*itemPerpage){
		  			arr.push(e)
		  		}
		  	})	  
		  	state.paginateProjects = state.paginateProjects.concat(arr);		
	  		}else{
	  			state.projects.forEach((e,x)=>{
		  		if(x >=(action.payload['paginationNum']-1)*itemPerpage && x < action.payload['paginationNum']*itemPerpage){
		  			arr.push(e)
			  		}
			  	})			  	
			  	state.paginateProjects = state.paginateProjects.concat(arr)			  	
  			} 	
  			if(state.paginateProjects.length > 1){
					state.currentPagination = action.payload['paginationNum'] 
				}else {
					state.currentPagination = action.payload['paginationNum']-1;
				}
  			return {...state};	
  			case 'CLOSED_PROJECTS': 
  			state.projects = state.imutateProjects; 			
  			state.projects = state.projects.filter((e)=>{
		        return e['is_complete'] == "1";
		      });   						
			return {...state}
			case 'GET_TRASHED_PROJECTS': 
  			state.projects = state.imutateProjects; 			
  			state.projects = state.projects.filter((e)=>{
		        return e['is_trashed'] == "1";
		      });   						
			return {...state}
			case 'REOPEN_PROJECT':
			state.projects.forEach((e,i)=>{
				if(e['id'] == action.payload['id']){
					e['is_complete'] = "0";
					alert(e['id'])
				}
			})							
			return {...state}	
			case 'All_PROJECTS':			
			state.projects = state.imutateProjects;
			state.projects = state.projects.filter((e)=>{
		        if(e['is_complete'] == "0" && e['is_trashed']=="0"){
		        	return e;
		        }
		      });			
			return {...state}; 	
			case 'CLOSE_PROJECT':
			state.projects.forEach((e,i)=>{
				if(e['id'] == action.payload['id']){
					e['is_complete'] = "1";
				}
			})
			return {...state};
			case'GET_PROJECT':
			state.projects.forEach((e,i)=>{
				if(e['id'] == action.payload['id']){
					state.project  = e;
					if(e['is_complete'] == '0'){
						state.is_complete = false
					}else if(e['is_complete'] == '1'){
						state.is_complete = true;
					}					
				}
			})			
			return {...state}
			case 'GET_COMMENT':
			state.comment = [];
			state.comment = action.payload['comment'];			
			return {...state};
			case 'ADD_COMMENT':
			state.comment.push(action.payload);
			return {...state};
			case 'SEARCH_PROJECS':
			state.projects = state.imutateProjects;
			if(action['payload']['sort'] == 'sort_by_all_open_projects'){
				state.projects = state.projects.filter((e) =>{
					return e['is_complete'] == '0' && e['is_trashed']=="0" ;
				})
			}
			return {...state}
			case 'SEARCH_PROJECT_BY_NAME':
			state.projects = state.imutateProjects;
			if(action.payload['type'] == 'default'){				
				state.projects = state.projects.filter((e)=>{
				return e['title'].toLowerCase().indexOf(action['payload']['title'].toLowerCase()) !== -1 ;
				})
				state.searchResult = state.projects;
			}else if(action.payload['type'] == 'member_all'){	
					//        OLD PROCESS			// 
				// state.projects = state.projects.filter((e)=>{
				// return e['total_member'].map((x) => {return x['member_name'].toLowerCase()}).indexOf(action.payload['member_name'].toLowerCase()) !== -1;
				// })
				let index:Array<any> = [];
				state.projects.forEach((e,i)=>{
					let hasMember:boolean = false;
					e['total_member'].forEach((member)=>{
						console.log(member['member_name'].toLowerCase())
						if(member['member_name'].toLowerCase().search(action.payload['member_name'].toLowerCase()) > -1){
							hasMember = true;
							return false;
						}	
					})					
					if(hasMember){
						index.push(e);						
					}					
				})				
				state.projects = index;

			}else if(action.payload['type'] == 'admin_all'){				
				let index:Array<any> = [];
				state.projects.forEach((e,i)=>{
					let hasMember:boolean = false;
					e['total_admin'].forEach((member)=>{						
						if(member['admin_name'].toLowerCase().search(action.payload['admin_name'].toLowerCase()) > -1){
							hasMember = true;
							return false;
						}	
					})					
					if(hasMember){
						index.push(e);						
					}					
				})				
				state.projects = index;

			}else if(action.payload['type'] == 'default_open'){	
				state.projects = state.projects.filter((p)=>{
					return p['is_complete'] == '0' && p['is_trashed'] == '0';
				})			
				state.projects = state.projects.filter((e)=>{
				return e['title'].toLowerCase().indexOf(action['payload']['title'].toLowerCase()) !== -1 ;
			})
			}else if(action.payload['type'] == 'member_open'){	
				state.projects = state.projects.filter((p)=>{
					return p['is_complete'] == '0' && p['is_trashed'] == '0';
				})			
				let index:Array<any> = [];
				state.projects.forEach((e,i)=>{
					let hasMember:boolean = false;
					e['total_member'].forEach((member)=>{
						console.log(member['member_name'].toLowerCase())
						if(member['member_name'].toLowerCase().search(action.payload['member_name'].toLowerCase()) > -1){
							hasMember = true;
							return false;
						}	
					})					
					if(hasMember){
						index.push(e);						
					}					
				})				
				state.projects = index;
			}else if(action.payload['type'] == 'admin_open'){	
				state.projects = state.projects.filter((p)=>{
					return p['is_complete'] == '0' && p['is_trashed'] == '0';
				})			
				let index:Array<any> = [];
				state.projects.forEach((e,i)=>{
					let hasMember:boolean = false;
					e['total_admin'].forEach((member)=>{						
						if(member['admin_name'].toLowerCase().search(action.payload['admin_name'].toLowerCase()) > -1){
							hasMember = true;
							return false;
						}	
					})					
					if(hasMember){
						index.push(e);						
					}					
				})				
				state.projects = index;
			}else if(action.payload['type'] == 'default_close'){	
				state.projects = state.projects.filter((p)=>{
					return p['is_complete'] == '1' && p['is_trashed'] == '0';
				})			
				state.projects = state.projects.filter((e)=>{
				return e['title'].toLowerCase().indexOf(action['payload']['title'].toLowerCase()) !== -1 ;
			})
			}else if(action.payload['type'] == 'member_close'){	
				state.projects = state.projects.filter((p)=>{
					return p['is_complete'] == '1' && p['is_trashed'] == '0';
				})			
				let index:Array<any> = [];
				state.projects.forEach((e,i)=>{
					let hasMember:boolean = false;
					e['total_member'].forEach((member)=>{
						console.log(member['member_name'].toLowerCase())
						if(member['member_name'].toLowerCase().search(action.payload['member_name'].toLowerCase()) > -1){
							hasMember = true;
							return false;
						}	
					})					
					if(hasMember){
						index.push(e);						
					}					
				})				
				state.projects = index;
			}else if(action.payload['type'] == 'admin_close'){	
				state.projects = state.projects.filter((p)=>{
					return p['is_complete'] == '1' && p['is_trashed'] == '0';
				})			
				let index:Array<any> = [];
				state.projects.forEach((e,i)=>{
					let hasMember:boolean = false;
					e['total_admin'].forEach((member)=>{						
						if(member['admin_name'].toLowerCase().search(action.payload['admin_name'].toLowerCase()) > -1){
							hasMember = true;
							return false;
						}	
					})					
					if(hasMember){
						index.push(e);						
					}					
				})				
				state.projects = index;
			}else if(action.payload['type'] == 'default_trashed'){	
				state.projects = state.projects.filter((p)=>{
					return p['is_trashed'] == '1';
				})			
				state.projects = state.projects.filter((e)=>{
				return e['title'].toLowerCase().indexOf(action['payload']['title'].toLowerCase()) !== -1 ;
			})
			}else if(action.payload['type'] == 'member_trashed'){	
				state.projects = state.projects.filter((p)=>{
					return p['is_trashed'] == '1';
				})			
				let index:Array<any> = [];
				state.projects.forEach((e,i)=>{
					let hasMember:boolean = false;
					e['total_member'].forEach((member)=>{
						console.log(member['member_name'].toLowerCase())
						if(member['member_name'].toLowerCase().search(action.payload['member_name'].toLowerCase()) > -1){
							hasMember = true;
							return false;
						}	
					})					
					if(hasMember){
						index.push(e);						
					}					
				})				
				state.projects = index;
			}else if(action.payload['type'] == 'admin_trashed'){	
				state.projects = state.projects.filter((p)=>{
					return p['is_trashed'] == '1';
				})			
			let index:Array<any> = [];
				state.projects.forEach((e,i)=>{
					let hasMember:boolean = false;
					e['total_admin'].forEach((member)=>{						
						if(member['admin_name'].toLowerCase().search(action.payload['admin_name'].toLowerCase()) > -1){
							hasMember = true;
							return false;
						}	
					})					
					if(hasMember){
						index.push(e);						
					}					
				})				
				state.projects = index;
			}
			return state;

			case 'GET_WORKSPACE':			
			state.allworkspace.forEach((ws)=>{
				if(ws.id == action.payload['id']){
					state.currentWorkspace = ws
				}
			})

			return {...state};
			case 'MODAL_OPEN':
			state.modalOpen = true;
			state.showModalContent = 'add-project'
			console.log(state)
			return {...state}
			case 'MODAL_CLOSE':
			state.modalOpen = false;
			state.showModalContent = ''
			return {...state}
			case 'WORKSPACE_MODAL':
			state.modalOpen = true;
			state.showModalContent = 'add-workspace';
			return {...state}
			case 'WORKSPACE_ADD':
			let newWorkspace = {
				'id':action.payload['ws_id'],
				'title':action.payload['title'],
				'desc':action.payload['desc']
			}
			state.activeWorkspace.push(newWorkspace);
			return {...state}
			case 'WORKSPACE_DELETE':
			state.activeWorkspace = state.activeWorkspace.filter((e)=>{
				return e['id'] !== action.payload['ws_id']
			})
			return {...state}
		default:
			return state;
	}
}
