import { Component, OnInit,ViewChildren,AfterViewInit,ElementRef,QueryList} from '@angular/core';
import {getProjectService} from '../service/get-project.service';
import {MiniProjects} from '../class/mini-projects';
import {Router} from'@angular/router';
import {RouterParamsCatcherService} from '../service/router-params-catcher.service';
import {BannerComponent} from '../common-view/banner/banner.component';
import {TabComponent} from '../tab-component/tab-component.component';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit,AfterViewInit {

   private currentYear:number;
   private currentMonth:number;
   private currentDay:number;
   private date:Date;

  private tab:Array<any> = [{text:'Today'},{text:'Last Week'},{text:'Last Month'},{text:'Last year'}]
  private projectArr:MiniProjects[] = [];
  private projectArrMutate:MiniProjects[] = [];
  private project:MiniProjects;

  @ViewChildren('ngTab') ngTab:QueryList<'ngTab'>;

  ngAfterViewInit(){

    this.projectList.getAllPublicProject()
    .subscribe(
      data=>{
         console.log(data);

        for(let project in data){
           // console.log(data[project]);

          let projectId = (data[project].hasOwnProperty('id'))?data[project]['id']:'';          
          let title = (data[project].hasOwnProperty('title'))?data[project]['title']:''; 
          let  desc  = (data[project].hasOwnProperty('description'))?data[project]['description']:'';
          let startDate =(data[project].hasOwnProperty('start_date'))?data[project]['start_date']:'';
          let  dueDate =(data[project].hasOwnProperty('end_date'))?data[project]['end_date']:'';
          let  adminId = (data[project].hasOwnProperty('creator_id'))?data[project]['creator_id']:'';
          let  teamId = (data[project].hasOwnProperty('team_id'))?data[project]['team_id']:'';
          let  is_public = (data[project].hasOwnProperty('is_public'))?data[project]['is_public']:'';
          let  status = (data[project].hasOwnProperty('status'))?data[project]['status']:'';

           this.project = new MiniProjects(projectId,title,desc,false,false,dueDate,startDate);
           this.projectArr.push(this.project);

            //console.log(this.projectArr)
        }
        console.log(this.ngTab);
            // TAB     
    this.ngTab.forEach((d,i)=>{     
    if(d.hasOwnProperty('nativeElement'))  {  
      d['nativeElement'].addEventListener('click',()=>{
       this.ngTab.forEach((a)=>{
         a['nativeElement'].classList.remove('active')
      })
         d['nativeElement'].classList.add("active")

         if(d['nativeElement'].innerText  == 'Today'){           
          this.projectArrMutate =  this.projectArr.filter((project)=>{
             let date = new Date(project.startDate)
             console.log(new Date(project.startDate),new Date(project.startDate).getDate())  
             console.log(this.currentDay)
             if(date.getUTCDate() == this.currentDay)
                return project;
             })         
              console.log(this.projectArr)
         }else if(d['nativeElement'].innerText  == 'Last Month'){
           this.projectArrMutate = this.projectArr.filter((project)=>{
             let date = new Date(project.startDate)
             console.log(date.getMonth())  
             console.log(this.currentMonth-1)
             if(date.getMonth() == this.currentMonth-1 )
                return project;
             })
              console.log(this.projectArr)
         }

      })
      if(i == 0){
        d['nativeElement'].click();
      }
    }
    })
      },
      error =>{
        console.log(error);
      }
      )  
  }
  constructor(private projectList:getProjectService,private router:Router,private projectIdCatch:RouterParamsCatcherService) {

    this.date = new Date();
    this.currentDay = this.date.getDate();
    this.currentYear = this.date.getFullYear();
    this.currentMonth = this.date.getMonth();
   }

   private openProject(id){
     this.projectIdCatch.set_id(id);              
     this.router.navigate(['project/'+id+'']);   
   }

  ngOnInit() {
  }

}
