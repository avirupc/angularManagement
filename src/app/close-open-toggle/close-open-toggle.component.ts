import { Component, OnInit,Input } from '@angular/core';
import {AddProjectInfoService} from '../service/add-project-info.service';
import {Store} from '@ngrx/store';
import {AppState} from '../state/app-state';
import {allAction,closedProjects,paginate,closeProject,reOpenProject,getAllProjects} from '../Action/action';
@Component({
  selector: 'app-close-open-toggle',
  templateUrl: './close-open-toggle.component.html',
  styleUrls: ['./close-open-toggle.component.css']
})
export class CloseOpenToggleComponent implements OnInit {

	private state:any;

	@Input() project:any;
	@Input() inSearchPage:boolean;

  constructor(private projectManipulate:AddProjectInfoService,private store:Store<AppState>) { }

  ngOnInit() {

  	this.state = this.store.select('mainState');
  }

  checkValue(event:any,id:any){
  	 if(event.currentTarget.checked){
       this.projectManipulate.closeProject({'project_id':id})
       .subscribe(data =>{
         console.log(data);
         if(data['status'] == 'Success'){
           this.store.dispatch(new closeProject({'id':id}))
           if(!this.inSearchPage){
            this.store.dispatch(new getAllProjects({}));
      		 }
           this.store.dispatch(new paginate({'paginationNum':1,'paginationId':0})) 
         }
       })
     }else{
     	this.projectManipulate.reOpenProject({'project_id':id})
     	       .subscribe(data =>{
         console.log(data);
         if(data['status'] == 'Success'){
           this.store.dispatch(new reOpenProject({'id':id})) 
           if(!this.inSearchPage){
           this.store.dispatch(new closedProjects({}))
      		 }
           this.store.dispatch(new paginate({'paginationNum':1,'paginationId':0}))
         }
       })
     } 
  }

}
