import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CloseOpenToggleComponent } from './close-open-toggle.component';

describe('CloseOpenToggleComponent', () => {
  let component: CloseOpenToggleComponent;
  let fixture: ComponentFixture<CloseOpenToggleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CloseOpenToggleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CloseOpenToggleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
