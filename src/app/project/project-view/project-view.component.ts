import { Component, OnInit,Output } from '@angular/core';
import {getProjectService} from '../../service/get-project.service';
import {Router,ActivatedRoute} from '@angular/router';
import {RouterParamsCatcherService} from '../../service/router-params-catcher.service';
import {Project} from '../../class/project';
import {ProjectGurdsService} from '../../projectGurds/project-gurds.service';


@Component({
  selector: 'app-project-view',
  templateUrl: './project-view.component.html',
  styleUrls: ['./project-view.component.css']
})
 export class ProjectViewComponent implements OnInit {

 	id:number;
	title:string;
	desc:string;
	dueDate:Date;
	startDate:Date;
	creatorId:number;	
	is_public:boolean;
	status:string;
	member:any[];
	admin:any[];
	team:any[];
	task:any[];
	private isLogin:boolean;
	private userID:number;
	private is_member:boolean;
	private is_admin:boolean;	
	private project:Project;

  constructor(private projectDetails:getProjectService, private projectIdCatch:RouterParamsCatcherService,private router:Router,private route:ActivatedRoute) { }

  ngOnInit() {  

  	// Determine isLogedIn or not

  	let islogin = localStorage.getItem('isLogin');
  	if(islogin){
  		if(islogin == 'true'){
  			let userid = localStorage.getItem('id');
  			if(userid && parseInt(userid) > 0){
  				this.isLogin = true;
  				this.userID = parseInt(userid);
  				this.projectIdCatch.set_user_id(this.userID);
  				let data = {userId:this.userID,projectId:this.projectIdCatch.get_id()};
  				this.projectDetails.logedInUserIsMember(JSON.stringify(data))
  				.subscribe(
  					data=>{
  						console.log(data);
  						if(data.hasOwnProperty('is_member')){
  							if(data['is_member'] == 'true'){
  								this.is_member = true;
  							}else if(data['is_member'] == 'false'){
  								this.is_member = false;
  							}
  						}
  						if(data.hasOwnProperty('is_admin')){
  							if(data['is_admin'] == 'true'){
  								this.is_admin = true;
  							}else if(data['is_admin'] == 'false'){
  								this.is_admin = false;
  							}
  						}
  					},
  					error=>{
  						console.log(error);
  					}
  					)
  			}else{
  				this.isLogin = false;
  				this.userID = 0;
  			}
  		}else{
  			this.isLogin = false;
  				this.userID = 0;
  		}
  	}else{
  		this.isLogin = false;
  				this.userID = 0;
  	}  	

  	this.projectDetails.getProjectDetails(this.projectIdCatch.get_id())
  	.subscribe(
  		data=>{
  			console.log(data);
  			if(data.hasOwnProperty('status')){
  				if(data['status'] == true && data.hasOwnProperty('project')) {

  					this.id = data['project'].hasOwnProperty('id')? data['project']['id']:'';
					this.title =  data['project'].hasOwnProperty('title')? data['project']['title']:'';
					this.desc= data['project'].hasOwnProperty('description')? data['project']['description']:'';
					this.dueDate = data['project'].hasOwnProperty('end_date')? data['project']['end_date']:'';
					this.status = data['project'].hasOwnProperty('status')? data['project']['status']:'';
					this.startDate = data['project'].hasOwnProperty('start_date')? data['project']['start_date']:'';
					this.member = data.hasOwnProperty('member')? data['member']:'';
					this.admin = data.hasOwnProperty('admin')? data['admin']:'';
					this.team = data.hasOwnProperty('team')? data['team']:'';
					this.task = data.hasOwnProperty('task')? data['task']:'';

					if(data['project'].hasOwnProperty('is_public') && data['project']['is_public'].length >0){

						if(data['project']['is_public'] == '1'){

							this.is_public =true;

						}else if(data['project']['is_public'] == '0'){

							this.is_public = false;
						}
					}
					this.creatorId =data['project'].hasOwnProperty('creator_id')? data['project']['creator_id']:'';
					
					this.project = new Project(this.title,this.desc,this.dueDate,this.startDate,this.creatorId,this.is_public,this.status,this.member,this.admin,this.team,this.task,this.id);
					console.log(this.project);
  				}
  			}
  		}

  		)  	
  }

  private openTask(id:any):void{
  	this.router.navigate(['task/'+id+''],{relativeTo: this.route});
  }

}
