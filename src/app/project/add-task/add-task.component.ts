import { Component, OnInit } from '@angular/core';
import {AddTaskService} from '../../service/add-task.service';
import {RouterParamsCatcherService} from '../../service/router-params-catcher.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.css']
})
export class AddTaskComponent implements OnInit {

	private projectId:any;
	private urlParams:string;
	private url:string[];
	private logedUserId:any;
	private task:Object;

  constructor(private addtask:AddTaskService,private projectIdCatch:RouterParamsCatcherService,private router:Router) { }

  ngOnInit() {
  	this.urlParams = window.location.href.replace(window.location.origin,'');
           this.url = this.urlParams.split('/');  
           console.log(this.url); 
       
           if(this.url[1] == 'project' && this.url[2].length > 0){                   
               if(parseInt(this.url[2]) > 0){
                 this.projectIdCatch.set_id(this.url[2])
                }
            }    

            console.log(this.projectIdCatch);  

            this.projectId = this.projectIdCatch.get_id();
            this.logedUserId = this.projectIdCatch.get_user_id();

  }

  private addTask(info){

  	 let projectId = this.projectId;
  	 let title = info.value.taskName;
  	 let desc = info.value.desc;
  	 let startDate = new Date();
  	 let dueDate = info.value.dueDate;
  	 let creatorId = this.logedUserId;
  	 let status = 'nextinline';
  	 this.task = {'projectId':projectId,'title':title,'desc':desc,'startDate':startDate,'dueDate':dueDate,'creatorId':creatorId,'status':status};
  	 console.log(info.value);

  	 this.addtask.addTask(JSON.stringify(this.task))
  	 .subscribe(
  	 	data=>{
  	 		console.log(data);
  	 		if(data.hasOwnProperty('status') && data['status'] == 'Success' ){
  	 			this.router.navigate(['/project/'+this.projectId+''])
  	 		}
  	 	},
  	 	error =>{
  	 		console.log(error);
  	 	}

  	 	)
  	 ;
  }

}
