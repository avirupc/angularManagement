import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ProjectGurdsService} from '../projectGurds/project-gurds.service';
import {ProjectViewComponent} from './project-view/project-view.component';
import { AddTaskComponent } from './add-task/add-task.component';
import {RouterModule} from '@angular/router';
import {FormsModule} from '@angular/forms';
import { TaskListComponent } from './task-list/task-list.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule
  ],
  declarations: [ProjectViewComponent, AddTaskComponent, TaskListComponent],
  bootstrap: [ProjectViewComponent] 
})
export class ProjectModule { }
