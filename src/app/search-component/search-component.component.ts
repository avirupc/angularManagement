import { Component, OnInit,Input } from '@angular/core';
import {Observable,Subject} from 'rxjs';
import {Store} from '@ngrx/store';
import {AppState} from '../state/app-state';
import {allAction ,getComment,addComment,searchProject,paginate,searchProjectByName} from '../Action/action';
import {Router} from '@angular/router';

@Component({
  selector: 'app-search-component',
  templateUrl: './search-component.component.html',
  styleUrls: ['./search-component.component.css']
})
export class SearchComponentComponent implements OnInit {

  private state:any;
  private showDropDown:boolean=false;
  private showDropDown_2:boolean  = false;
  private serach_by_text:string = 'search by project name';
  private search_by_keyword:string = 'project name';
  private serach_in_text:string = 'Sarch in all projects';
  private search_in_keyword:string = 'all projects';
  public keyWord:string = '';
	_data = new Subject();

  _loggedInUser = new Subject();
  loggedInUser:any;

  @Input()

  public set authUser(v:any){
    this._loggedInUser.next(v);
  }

  public get authUser(){
    return this._loggedInUser;
  }

	@Input() 

	public set ArrayData(v : any) {
		this._data.next(v);
	}

	public get ArrayData() : any {
		return this._data;
	}

  constructor(private store:Store<AppState>,private rouer:Router) { 

  	this._data.subscribe(data =>{
    //   console.clear()
  		// console.log(data)
  	})  	

    this._loggedInUser.subscribe((data)=>{
      this.loggedInUser = data;
    })
    this.state = this.store.select('mainState');

  }
  search(keyWord:any){      
    if(keyWord.length > 0){
      // alert(this.search_by_keyword)
    if(this.search_in_keyword == 'all projects'){    
        if(this.search_by_keyword == 'project name'){ 
          this.store.dispatch(new searchProjectByName({'title':keyWord,'type':'default'}));
        }else if(this.search_by_keyword == 'member name'){
          this.store.dispatch(new searchProjectByName({'member_name':keyWord,'type':'member_all'}));
        }else if(this.search_by_keyword == 'admin name'){
          this.store.dispatch(new searchProjectByName({'admin_name':keyWord,'type':'admin_all'}));
        }        
    }else if(this.search_in_keyword == 'open projects'){
        if(this.search_by_keyword == 'project name'){ 
          this.store.dispatch(new searchProjectByName({'title':keyWord,'type':'default_open'}));
        }else if(this.search_by_keyword == 'member name'){
          this.store.dispatch(new searchProjectByName({'member_name':keyWord,'type':'member_open'}));
        }else if(this.search_by_keyword == 'admin name'){
          this.store.dispatch(new searchProjectByName({'admin_name':keyWord,'type':'admin_open'}));
        } 
    }else if(this.search_in_keyword == 'closed projects'){
        if(this.search_by_keyword == 'project name'){ 
          this.store.dispatch(new searchProjectByName({'title':keyWord,'type':'default_close'}));
        }else if(this.search_by_keyword == 'member name'){
          this.store.dispatch(new searchProjectByName({'member_name':keyWord,'type':'member_close'}));
        }else if(this.search_by_keyword == 'admin name'){
          this.store.dispatch(new searchProjectByName({'admin_name':keyWord,'type':'admin_close'}));
        } 
    }else if(this.search_in_keyword == 'trashed projects'){
        if(this.search_by_keyword == 'project name'){ 
          this.store.dispatch(new searchProjectByName({'title':keyWord,'type':'default_trashed'}));
        }else if(this.search_by_keyword == 'member name'){
          this.store.dispatch(new searchProjectByName({'member_name':keyWord,'type':'member_trashed'}));
        }else if(this.search_by_keyword == 'admin name'){
          this.store.dispatch(new searchProjectByName({'admin_name':keyWord,'type':'admin_trashed'}));
        } 
    }    
    else{           
      console.log(this.search_by_keyword);
      this.store.dispatch(new searchProject({'sort':'sort_by_all_open_projects'}));
      this.store.dispatch(new paginate({'paginationNum':1,'paginationId':0}));
    }       
    }       
    // DISPATCH PAGINATES
    this.store.dispatch(new paginate({'paginationNum':1,'paginationId':0}));
    this.rouer.navigate(['dashboard/'+this.loggedInUser+'/user-project/search']);
  }
  searchBy(keyWord:any){
    this.search_by_keyword = keyWord;
    this.serach_by_text = 'Search by '+keyWord;
    this.showDropDown = !this.showDropDown;
  }
    searchIn(keyWord:any){
    this.search_in_keyword = keyWord;
    this.serach_in_text = 'Search in '+keyWord;
    this.showDropDown_2 = !this.showDropDown_2;
  }


  toggleDropDown(searchCat:string){
    if(searchCat == 'project_search_by'){
      this.showDropDown = !this.showDropDown;
      this.showDropDown_2  = false;
    }else if(searchCat == 'project_search_in'){
      this.showDropDown_2 =! this.showDropDown_2;
      this.showDropDown = false;
    }
  }

  ngOnInit() {
  }

}
