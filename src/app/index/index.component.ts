import { Component, OnInit,ViewChild,ElementRef,AfterViewInit,OnChanges,AfterContentInit,ComponentRef } from '@angular/core';
import {Router} from '@angular/router';
import {AppState} from '../state/app-state';
import {Observable} from 'rxjs';
import {Store} from'@ngrx/store';
import {allAction,Login,Logout} from '../Action/action';
import {RouterParamsCatcherService} from '../service/router-params-catcher.service';
import {SharedLogoutService} from '../service/shared-logout.service';
import {ModalComponent} from '../modal/modal.component';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit,AfterViewInit,OnChanges,AfterContentInit {



	@ViewChild('any') component:ComponentRef<any>
 
	ngAfterViewInit(){		
			 
	}

	ngAfterContentInit(){
		console.log(this.component)
		
	}
  state:Observable<AppState>;
  templateState:AppState
  action:Login;
  isLogin;
  userId;
  inDashboard:boolean;

  constructor(private store:Store<AppState>,private router:Router,private userCatcher:RouterParamsCatcherService,private sharedLogout:SharedLogoutService)  {	
  	sharedLogout.logoutObservable.subscribe((data)=>{
        this.store.dispatch(new Logout(data));   
    })
  	this.router.events.subscribe(event => {
    if (event.constructor.name === 'NavigationStart') {
     // this.loader.nativeElement.style.display = 'block'
    }  
  });
	
   this.state = store.select('mainState')

   if(localStorage.getItem('isLogin') && localStorage.getItem('isLogin') == 'true'){

   		if(localStorage.getItem('id') && parseInt(localStorage.getItem('id')) > 0){
   			 store.dispatch(new Login({'isLogin':true,userId:localStorage.getItem('id')}))   			 
   		}else{
   			store.dispatch(new Logout({'isLogin':false,userId:''}))
   		}
      }else{
         	store.dispatch(new Logout({'isLogin':false,userId:''}))
      }  

    this.state.subscribe(data =>{
   	this.templateState = data;
   	console.log(data)
   	this.isLogin = data.isLogin
   	this.userId = data.userId;     
   })
   }

   logout(event){
   	this.store.dispatch(new Logout({'isLogin': false,userId:''}));    
   }

  ngOnInit() {
  	
  }
  ngOnChanges(){
  	
  }
  onActivate(component:ComponentRef<any>){  	
  	console.log(component);  
  	if(component.constructor.name == 'LoginComponent' )	{
  		component['isLogin'] = true;
      this.inDashboard = false
  	}else if(component.constructor.name == 'UserDashboardComponent'){
  		this.inDashboard = true; 
      component['loggedInUser'] = this.userId ; 		
  	}else{
  		this.inDashboard = false
  	}
 

}
}