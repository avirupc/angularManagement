import { Component, OnInit,Input} from '@angular/core';

@Component({
  selector: 'app-tab-component',
  templateUrl: './tab-component.component.html',
  styleUrls: ['./tab-component.component.css']
})
export class TabComponent implements OnInit {

	@Input() projectList:Array<any>;	

  constructor() { }

  ngOnInit() {
  }

}
