import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AppRouterModule} from '../app-router/app-router.module';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { SearchbarComponent } from './searchbar/searchbar.component';
import{FormsModule} from '@angular/forms';
import {ReactiveFormsModule} from '@angular/forms';
import { BannerComponent } from './banner/banner.component';
import {RegistrationModule} from '../registration/registration.module';
import {StoreModule} from '@ngrx/store';
import {appReducer} from '../reducer/app-reducer';

@NgModule({
  imports: [
    CommonModule,
    AppRouterModule,
    FormsModule,
    ReactiveFormsModule,
    RegistrationModule,
    StoreModule.forRoot({mainState:appReducer})
  ],
  exports:[HeaderComponent,FooterComponent,SearchbarComponent,BannerComponent],
  declarations: [HeaderComponent, FooterComponent, SearchbarComponent, BannerComponent]
})
export class CommonViewModule { }
