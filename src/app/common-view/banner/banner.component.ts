import { Component, OnInit,ViewChild,ComponentRef,AfterViewInit} from '@angular/core';
import {LoginComponent} from '../../registration/login/login.component';
@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.css']
})
export class BannerComponent implements OnInit,AfterViewInit {

	isLogin:boolean = false;
  isHome:boolean = true;

  @ViewChild('login') logincomponent:ComponentRef<any>

  ngAfterViewInit(){
    if(!this.isLogin){
      console.log(this.logincomponent);
    this.logincomponent['isHome'] = true;
    }
    
  }

  constructor() { }

  ngOnInit() {
  	if(localStorage.getItem('isLogin') && localStorage.getItem('isLogin') == 'true'){
  		if(localStorage.getItem('id') && parseInt(localStorage.getItem('id')) > 0){
  			this.isLogin = true;
  		}
  	}
  }

}
