import { Component, OnInit,Input,Output,EventEmitter,ElementRef } from '@angular/core';
import {Router,NavigationEnd } from '@angular/router';
import {SearchbarComponent} from '../searchbar/searchbar.component';
import {Store} from '@ngrx/store';
import {AppState} from '../../state/app-state';
import * as action from '../../Action/action';
import {Observable} from 'rxjs';
import {
   AuthService,   
   GoogleLoginProvider
} from 'angular-6-social-login';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  private showDropdown:boolean = false;

  @Input() isLogin:boolean;
  @Input() userId:any;

  @Output()
  isLogout = new EventEmitter();

  // isLogin;



  state:Observable<AppState>

  inDashboard:boolean = false;
  
  constructor(private router:Router,private store:Store<AppState>,private socialAuthService:AuthService) {

  			router.events.subscribe(data=>{  				
  				if(data instanceof NavigationEnd){
  					console.log(data.url)
  					if(data.url == '/dashboard'){
  						this.inDashboard = true;
  					}
  				}
  			})
        let self  = this;
        const body = document.getElementsByTagName('body')[0];
        body.addEventListener('click',function(e){
          // e.preventDefault();
          if(e.target['classList'][0] !== 'ng-nav-link'){
           self.showDropdown = false;
          }

        })

        console.log(this.userId)
        console.log(this.isLogin)

        // console.log(this.isLogin+'sfsgsdgsdg')

        // this.state = store.select('mainState');
        // this.state.subscribe(data =>{
        //   this.isLogin = data.isLogin
        // })
       }

   private  logout(){
   	localStorage.removeItem('isLogin');
   	localStorage.removeItem('id');
   	localStorage.removeItem('email');
     
     // this.store.dispatch(new action.Logout({isLogin:false}))

     // this.state.subscribe(data =>{
     //   this.isLogin = data.isLogin;

     //   console.log(data);
     // })
     // this.socialAuthService.signOut().then(function(data){
     //   console.log(data);
     // });
     this.socialAuthService.authState.subscribe(data=>{
       console.log(data);
     })
     // console.log()
     this.isLogout.emit({isLogin:false,userId:''});

   	this.router.navigate(['login']);

   }
   private showDropDown(){
     this.showDropdown = !this.showDropdown;
   }

  ngOnInit() {
  }

}
