import { CommonViewModule } from './common-view.module';

describe('CommonViewModule', () => {
  let commonViewModule: CommonViewModule;

  beforeEach(() => {
    commonViewModule = new CommonViewModule();
  });

  it('should create an instance', () => {
    expect(commonViewModule).toBeTruthy();
  });
});
