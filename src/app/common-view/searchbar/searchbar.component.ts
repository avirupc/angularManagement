import { Component, OnInit,Renderer2,ViewChild,AfterViewInit,ElementRef,Output,EventEmitter,Input} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {SearchMemberService} from '../../service/search-member.service';
import {SearchedUserListComponent} from '../../searched-user-list/searched-user-list.component';
import {UserList} from '../../interface/user-list';
import {FormControl} from '@angular/forms';
import {RouterParamsCatcherService} from '../../service/router-params-catcher.service';
import {Router} from '@angular/router';

import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-searchbar',
  templateUrl: './searchbar.component.html',
  styleUrls: ['./searchbar.component.css']
})
export class SearchbarComponent implements OnInit ,AfterViewInit{

  @ViewChild('searchInput') input:ElementRef;
  @Output() addedMember = new EventEmitter();
  @Output() valueChange = new EventEmitter();
  _data = new BehaviorSubject<any>([]);  

  @Input() 
  public set ArrayData(v : any) {
    this._data.next(v);
  }

  public get ArrayData() : any {
    return this._data;
  }
  @Input() selection_type:string;
 


  private userSearchQuery:FormControl = new FormControl();

  private user:UserList;
  public userArr:UserList[] = [];
  private userFound:boolean = false;
  private element:any;


  constructor(private searchMember:SearchMemberService,private router:Router,private routerParams:RouterParamsCatcherService,private rd:Renderer2) { }

  ngAfterViewInit() {} 



  ngOnInit() {      
    this.userSearchQuery.valueChanges
    .debounceTime(500)    
    .subscribe(result=>{      
      // console.log(result);
      if(result.length > 0){  
      this._data.subscribe(
      data=>{
        if(this.selection_type == 'member_selection'){
          this.userArr = [];
          if(data.hasOwnProperty('status') && data['status'] == 'Success'){ 
              this.userFound = true;
              data['user'].forEach((user)=>{
                this.userArr.push(user);
              })
          }else if(data['status'] == 'Not Found'){
              this.userFound = false; 
          }  
        }else if(this.selection_type == 'admin_selection'){
          this.userArr = [];
          if(data.hasOwnProperty('status') && data['status'] == 'Success'){ 
              this.userFound = true;
              console.log(data);
              data['user'].forEach((user)=>{
                this.userArr.push(user);
              })
          }else if(data['status'] == 'Not Found'){
              this.userFound = false; 
          }   
        }     
      },
      err=>{
        console.log(err);
      }
      )
    }else{
      this.userFound = false;
    }
    })
  }

  valueChanges(event){
    console.log(event);
    this.valueChange.emit({'value':event})
  }


    addUser(id:any,name:string){
      this.addedMember.emit({'userID':id,'userName':name})
    }

  


}
