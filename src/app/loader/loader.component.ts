import { Component, OnInit,OnDestroy } from '@angular/core';
import {LoaderService} from '../service/loader.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.css']
})
export class LoaderComponent implements OnInit , OnDestroy {

  constructor(private loader:LoaderService) { }

  private subscription:Subscription;
  private show:boolean = false;

  ngOnInit() {
  	this.subscription = this.loader.loaderState.subscribe(data =>{
  		this.show = data.show;
  	})
  }


  ngOnDestroy(){
  	this.subscription.unsubscribe();
  }

}
