import { Injectable } from '@angular/core';
import {Router,CanActivate} from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(private router:Router) { }

  canActivate():boolean{
  	let isLogin;
  	isLogin = localStorage.getItem('isLogin'); 

    // GET LOGED IN USER ID FROM LOCALSTORAGE

    let userId = localStorage.getItem('id');    

  	if(isLogin){
      if(userId.length > 0){
        return true;
      }  		
  	} 
  	this.router.navigate(['login']);	
  	return false;
  }
}
