import {Workspace} from '../class/workspace';
export interface AppState {
	isLogin:boolean;
	userId?:any;
	user?:any;
	projects?:any;
	imutateProjects?:any;
	closeProjects?:any;
	paginateProjects?:Array<any>;
	currentPagination?:any;
	totalPagination?:any;
	paginationArray?:any;
	project?:any;
	is_complete?:boolean;
	comment?:Array<any>;
	searchResult?:any;
	allworkspace:Array<any>;
	activeWorkspace :Array<any>;	
	currentWorkspace:any;
	modalOpen:boolean;
	showModalContent:string;
}
