import { Component, OnInit} from '@angular/core';
import {RouterParamsCatcherService} from '../service/router-params-catcher.service';
import {GetUserDetailsService} from '../service/get-user-details.service';
import {User} from '../class/user';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit{

private userId:any;	
private user:User;
private url:string[];

  constructor(private paramsCatcher:RouterParamsCatcherService,private getUser:GetUserDetailsService,private activRoute:ActivatedRoute) { }

    ngOnInit() {
  	alert();

  	this.activRoute.params.subscribe(data=>{
  		console.log(data);
  		this.userId = data.userId;
  		this.getUserDetails(this.userId);
  	})

  	this.url = window.location.href.replace(window.location.origin,'').split('/');
  	console.log(this.url);

  	if(this.url[1] == 'user' && this.url[2].length > 0){

  		if(parseInt(this.url[2]) > 0 ){
  			this.userId = parseInt(this.url[2]);
  		}
  	}else{
 	this.userId = this.paramsCatcher.get_id();
  	alert(this.userId);
  }
  	this.getUser.getUserData(JSON.stringify({userId:this.userId}))
  	.subscribe(

  		data=>{
  			console.log(data);
  			if(data.hasOwnProperty('status')){
  				if(data['status'] == 'Success'){

  					let id = data[0].hasOwnProperty('id')?data[0].id:'';	
		  			 let username = data[0].hasOwnProperty('login_name')?data[0].login_name:'';  			
		  			 let userpass = data[0].hasOwnProperty('login_pass')?data[0].login_pass:'';
		  			 let usernicename = data[0].hasOwnProperty('user_nicename')?data[0].user_nicename:'';
		  			 let useremail = data[0].hasOwnProperty('email')?data[0].email:'';

		  			 this.user = new User(id,username,userpass,usernicename,useremail,'');
		  			 console.log(this.user);
  				}
  			}
  		},
  		err =>{
  			console.log(err);
  		}
  		)
  	
  }
	getUserDetails(id:any){

	this.getUser.getUserData(JSON.stringify({userId:id}))
  	.subscribe(

  		data=>{
  			console.log(data);
  			if(data.hasOwnProperty('status')){
  				if(data['status'] == 'Success'){

  					let id = data[0].hasOwnProperty('id')?data[0].id:'';	
		  			 let username = data[0].hasOwnProperty('login_name')?data[0].login_name:'';  			
		  			 let userpass = data[0].hasOwnProperty('login_pass')?data[0].login_pass:'';
		  			 let usernicename = data[0].hasOwnProperty('user_nicename')?data[0].user_nicename:'';
		  			 let useremail = data[0].hasOwnProperty('email')?data[0].email:'';

		  			 this.user = new User(id,username,userpass,usernicename,useremail,'');
		  			 console.log(this.user);
  				}
  			}
  		},
  		err =>{
  			console.log(err);
  		}
  		)
	 }	

}
