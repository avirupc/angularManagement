import { Component, OnInit,Input,Output,EventEmitter,AfterViewInit } from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {Store} from '@ngrx/store';
import {AppState} from '../state/app-state';
import {paginate} from '../Action/action';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent implements OnInit,AfterViewInit {
  
  totalPagination:any;
  paginationArray:Array<any>=[];
  constructor(private store:Store<AppState>) {

    this.state = store.select('mainState');    
    this.state.subscribe(data=>{
      this.totalPagination = data.totalPagination;
      this.paginationArray = data.paginationArray      
    })

    }

  state:any;

  _data = new BehaviorSubject<any>([]); 
  itemPerPageArr:Array<Object> = [];
  outputArr:Array<any> = [];

  	@Output() nextPage = new EventEmitter();
  	@Input() itemPerpage:number;
    @Input() paginationNumber:any = 5;


    private hieghestNumber:number = this.paginationNumber;
    private lowestNumber:number = 0;
    
	@Input() 
	public set ArrayData(v : any) {
		this._data.next(v);
	}

	public get ArrayData() : any {
		return this._data;
	}

  ngOnInit() {
console.log('DULAL');
  	this._data.subscribe((data) =>{      		
  		// let counter = 0;
  		// for(var c in data){
  		// 	counter++;
  		// }
    //   this.itemPerPageArr = []
  		//  //let totalPagination = Math.ceil(counter/this.itemPerpage);
  		//  let counter2 = 0;
		  // 	for(var x=1 ; x <= this.totalPagination ; x++){		  		
		  // 		this.itemPerPageArr.push({id:counter2,'active':false,'number':x});
		  // 		counter2 ++;
		  // 	}
		  // 	console.log('DULAL');
		  	// alert()  	
  	})
  	 	// this.paginate(1,0);
  }

  ngAfterViewInit(){
   
  }

  paginate(num,event){

    this.store.dispatch(new paginate({'paginationNum':num,'paginationId':event}))

  	// for(var x = 0 ; x < this.itemPerPageArr.length ; x++){
  	// 	this.itemPerPageArr[x]['active'] = false;
  	// 	if(x == event){
  	// 		this.itemPerPageArr[x]['active'] = true;
  	// 	}
  	// }
  	// console.log(this.itemPerPageArr)
  	// this.outputArr = []
  	// this._data.subscribe((data) =>{  		
  	// 	let counter = 0;
  	// 	for(var c in data){
  	// 		counter++;
  	// 	}
  	// 	if(num == 1){
	  // 		for(var x =0 ; x < counter ; x++){
	  // 		if(x >=(num-1)*this.itemPerpage && x < num*this.itemPerpage){
	  // 			this.outputArr.push(data[x])
	  // 		}
	  // 		}
  	// 	}else{
	  // 		for(var x =0 ; x < counter ; x++){
	  // 		if(x >=(num-1)*this.itemPerpage && x < num*this.itemPerpage){
	  // 			this.outputArr.push(data[x])
	  // 		}
	  // 		}
  	// 	}

  	// })
  	 this.nextPage.emit(this.outputArr);
  }

  // paginates(currPage){

  //   let totalPages = Math.ceil(totalItems/this.itemPerpage);
  //   //ensure current page is not out of range

  //   if(currPage < 1){
  //     currPage = 1
  //   }else if(currPage > totalPages){
  //     currPage = totalPages;
  //   }

  //   let startPage;
  //   let endPage;

  //   if(totalPages <= this.paginationNumber){

  //     // total pages less tan paginationnumber so show all pages
  //     startPage = 1;
  //     endPage = totalPages;
  //   } else {
      
  //   }

  // }

}
