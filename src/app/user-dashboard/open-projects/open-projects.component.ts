import { Component, OnInit } from '@angular/core';
import {PaginationComponent} from '../../pagination/pagination.component';
import {Store} from'@ngrx/store';
import {AppState} from '../../state/app-state';
import {Observable} from 'rxjs';
import {sliceProjects,paginate,getAllProjects,closeProject} from '../../Action/action';
import {AddProjectInfoService} from '../../service/add-project-info.service';
@Component({
  selector: 'app-open-projects',
  templateUrl: './open-projects.component.html',
  styleUrls: ['./open-projects.component.css']
})
export class OpenProjectsComponent implements OnInit {
	public headerName:string = 'Open Projects';
	state:any;
	userProject:any;
	miniProjectArr:any = [];
	projectArr:Array<any>;


  constructor(private store:Store<AppState>,private projectManipulate:AddProjectInfoService) {
  	  this.state = this.store.select('mainState');    
      this.store.dispatch(new getAllProjects({}));  
     //this.store.dispatch(new sliceProjects({'itemPerpage':this.itemPerpage}))
      this.store.dispatch(new paginate({'paginationNum':1,'paginationId':0}))
      this.state.subscribe((data) =>{             
      this.miniProjectArr = data.projects
      this.projectArr = data.paginateProjects; 
      // console.log('OLA');
      console.log(this.projectArr)    
     })
   }
  ngOnInit() {
  }
}
