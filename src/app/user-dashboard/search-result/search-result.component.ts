import { Component, OnInit } from '@angular/core';
import {PaginationComponent} from '../../pagination/pagination.component';
import {Store} from'@ngrx/store';
import {AppState} from '../../state/app-state';
import {Observable} from 'rxjs';
import {sliceProjects,paginate,getAllProjects,closeProject} from '../../Action/action';
import {AddProjectInfoService} from '../../service/add-project-info.service';

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.css']
})
export class SearchResultComponent implements OnInit {

		state:any;
	userProject:any;
	miniProjectArr:any = [];
	projectArr:Array<any>;


  constructor(private store:Store<AppState>,private projectManipulate:AddProjectInfoService) { }

  ngOnInit() {
  	this.state = this.store.select('mainState');
  	// this.store.dispatch(new getAllProjects({}));  
     //this.store.dispatch(new sliceProjects({'itemPerpage':this.itemPerpage}))
      // this.store.dispatch(new paginate({'paginationNum':1,'paginationId':0}))
  	this.state.subscribe(data =>{
  		this.miniProjectArr = data.searchResult
      this.projectArr = data.paginateProjects;
  	})
  }

}
