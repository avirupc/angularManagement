import { Component, OnInit,Output,EventEmitter,Input } from '@angular/core';
import {  FileUploader, FileSelectDirective } from 'ng2-file-upload/ng2-file-upload';
import {AppState} from '../../state/app-state';
import {Store} from '@ngrx/store';
@Component({
  selector: 'app-uploader',
  templateUrl: './uploader.component.html',
  styleUrls: ['./uploader.component.css']
})
export class UploaderComponent implements OnInit {

	private url:string = 'http://quicktask.in/avirup/admin/user/addAttachmentinComment';
	public uploader: FileUploader = new FileUploader({url:this.url,itemAlias:'file'});

	state:any;
	logedInUser:any;

  constructor(private store:Store<AppState>) { }

  @Output() uploadComplete:EventEmitter<any> = new EventEmitter()
  @Input() projectId:any;

  ngOnInit() {

  		this.state = this.store.select('mainState');
  		this.state.subscribe(data =>{
  			if(data.isLogin)
  			this.logedInUser  = data.userId;
  		}) 

  	  	this.uploader.onBuildItemForm  = (file:any,form:any) =>{
  		form.append('user_id',this.logedInUser);
  		form.append('project_id',this.projectId);
  	}

  	this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false; };
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
         console.log(response);
         let data  = JSON.parse(response);
         if(data.hasOwnProperty('status') && data['status'].toLowerCase() == 'success'){
         	alert('File uploaded successfully');
         	this.uploadComplete.emit({'uploadContent':data})
         }else if(data.hasOwnProperty('status') && data['status'].toLowerCase() == 'failure'){
         	alert(data['message'])
         }

         
     };  
  }

}
