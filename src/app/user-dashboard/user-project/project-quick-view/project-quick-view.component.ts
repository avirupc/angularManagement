import { Component, OnInit,Input,Output,EventEmitter } from '@angular/core';
import{ActivatedRoute,Router} from '@angular/router';
import {Location} from '@angular/common';
import { NgForm } from '@angular/forms';
import {SearchbarComponent} from '../../../common-view/searchbar/searchbar.component';
import {AddProjectInfoService} from '../../../service/add-project-info.service';
import {DataObserverService} from '../../../service/data-observer.service';
import {GetUserDetailsService} from '../../../service/get-user-details.service';
import {Store} from '@ngrx/store';
import {AppState} from '../../../state/app-state';
import {allAction,updateTitle,addMember,updateDesc,updateDuedate,deleteMember,deleteProject,paginate,getAllProjects,getProject,reOpenProject,closedProjects,addAdmin,deleteAdmin} from '../../../Action/action';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { faPen,faPlus,faMinusCircle } from '@fortawesome/free-solid-svg-icons';
import {DeleteProjectService} from '../../../service/delete-project.service';
import {SearchMemberService} from '../../../service/search-member.service';
import { trigger, transition, useAnimation } from '@angular/animations';

@Component({
  selector: 'app-project-quick-view',
  templateUrl: './project-quick-view.component.html',
  styleUrls: ['./project-quick-view.component.css']   
})
export class ProjectQuickViewComponent implements OnInit {

  @Output() userDetails = new EventEmitter();

   project:any;
	 isOpen = true;
   miniProjectArr:any = [];   
   loggedInUser:any;
   lastProject:boolean= false;
   firstProject:boolean = false;
   userId:any;
   currPaginate:any;
   searchedUser:any;
   addAdmin:Object={'status':'Not Found','user':[]};   
   is_complete:boolean;


   // Edit Mode variable
   private editModeTitle:boolean = false;
   private editModeDesc:boolean=false;
   private editModeMember:boolean = false;
   private editModeDueDate:boolean = false;
   private editModeAdmin:boolean = false;

   private quickViewState:any;
   user:any;
   projectId:any;

  toggle() {
    this.isOpen = !this.isOpen;
  }
  faTimes = faTimes;
  faPen = faPen;
  faPlus = faPlus;
  faMinusCircle = faMinusCircle;

  constructor(private store:Store<AppState>,private route:ActivatedRoute,private router:Router,private updateProject:AddProjectInfoService,private getUser:GetUserDetailsService,private deleteProjectservice:DeleteProjectService,private searchMember:SearchMemberService) {  
    console.log(this.miniProjectArr); 
    console.log(this.project);    
   }

  ngOnInit() {
    this.quickViewState = this.store.select('mainState');
    this.route.params.subscribe(project=>{ 
      this.projectId = project.id;
      this.store.dispatch(new getProject({'id':project.id}));
      this.quickViewState.subscribe(data =>{
      this.currPaginate = data.currentPagination;
      this.loggedInUser = data.userId; 
      this.miniProjectArr = data['projects'];
      this.project = data.project;
      this.is_complete = data.is_complete; 
    })

    })   


    

  }

  query(event){
    console.log(event)
    this.searchMember.getUserList(JSON.stringify({userName:event.value})).subscribe(data =>{
      this.searchedUser = data;
    })
  }

  getMember(event){
    this.addAdmin['user'] = [];
    console.log(this.addAdmin['user'])
    this.project.total_member.forEach((e,i)=>{
      console.log(e['member_name'])
      // for(var x=0 ; x < event.value.length ; x++){
        for(var y=0 ; y < e['member_name'].length ; y++){
            if(event.value[y] !== 'undefined' && event.value[y] == e['member_name'][y]){
                this.addAdmin['status'] = 'Success';
                console.log(this.addAdmin)
                this.user = {};
                this.user = {'id':e['member_id'],'userName':e['member_name'],'image_link':e['user_image']} 
                let hasUser = false;
                this.addAdmin['user'].forEach((elem)=>{
                  if(elem['id'] == e['member_id']){
                    hasUser = true;
                  }
                })
                if(!hasUser){
                  this.addAdmin['user'].push(this.user);
                  this.addAdmin['index'] = i;
                }
                                               
            }else if(event.value[y] !== 'undefined' && event.value[y] !== e['member_name'][y]){
            if(this.addAdmin['user'].length == 0 ){
              this.addAdmin['status'] = 'Not Found';
            }else if(this.addAdmin['user'].length == 0 && this.addAdmin['index'] == i)   {
               this.addAdmin['status'] = 'Not Found';
            }           
              break;
            }else{
              break;
            }            
        }
    })
  }

  nextProject(id:string){
      for(var x=0 ; x < this.miniProjectArr.length ; x++){ 
        if(this.miniProjectArr[x]['id'] == id && x < (this.miniProjectArr.length-1)){  
        this.router.navigate(['/dashboard/'+this.loggedInUser+'/user-project/quickView/'+this.miniProjectArr[x+1]['id']+'']);
        this.lastProject = false;
        break;
        }else if(this.miniProjectArr[x]['id'] == id && x == (this.miniProjectArr.length-1)){
          this.lastProject = true;
        }
      }   
      this.editModeTitle = false;
      this.editModeDesc  = false;   
  }
  prevProject(id:string){

      for(var x=0 ; x < this.miniProjectArr.length ; x++){ 
        if(this.miniProjectArr[x]['id'] == id && x > 0){ 
        this.router.navigate(['/dashboard/'+this.loggedInUser+'/user-project/quickView/'+this.miniProjectArr[x-1]['id']+'']);
        this.firstProject = false;
        break;
        }else if(this.miniProjectArr[x]['id'] == id && x == 0){          
          this.firstProject  = true;
        }
      } 
  }

  getBack(){
    let parentPath = this.route.parent.routeConfig.path;
    this.router.navigate(['/dashboard/'+this.loggedInUser+'/user-project/'+parentPath+'']);
  }

  showEditmode(mode:string){
    if(mode == 'title'){
      this.editModeTitle = true;
      this.editModeDesc = false;
      this.editModeMember = false; 
      this.editModeDueDate = false;
      this.editModeAdmin = false;
    }else if(mode == 'desc'){
      this.editModeTitle = false;
      this.editModeDesc = true;
      this.editModeMember = false; 
      this.editModeDueDate = false;
      this.editModeAdmin = false;
    }else if(mode == 'member'){
      this.editModeTitle = false;
      this.editModeDesc = false;
      this.editModeMember = true; 
      this.editModeDueDate = false;
      this.editModeAdmin = false;
    }else if(mode =='duedate'){
      this.editModeDueDate = true;
      this.editModeTitle = false;
      this.editModeDesc = false;
      this.editModeMember = false;
      this.editModeAdmin = false;
    }else if(mode == 'admin'){
      this.editModeDueDate = false;
      this.editModeTitle = false;
      this.editModeDesc = false;
      this.editModeMember = false;
      this.editModeAdmin = true;

    }
  }

  cancelEditMode(mode:string,f:NgForm){
    if(mode == 'title'){
      this.editModeTitle = false;
    }else if(mode == 'desc'){
      this.editModeDesc = false;
    }else if(mode == 'member'){
      this.editModeMember = false;
    }else if(mode == 'duedate'){
      this.editModeDueDate = false;
    }
    f.resetForm(); 
  }
  saveData(f:NgForm,dataType:any,id:any){    
    if(f.valid){      
      if(id !== null ){        
        console.log(f.value);
        f.value['project_id'] = id;    
        if(dataType  == 'title'){          
          this.updateTitle(f.value.project_title,id);
        }else if(dataType == 'desc'){
          this.updateDescription(f.value['project_desc'],id)
        }else if(dataType == 'duedate'){          
          this.updateDuedate(f.value['project_duedate'],id)
        } 
        f.resetForm(); 
      }
    }    
  }

  updateDescription(desc:any,id:any){
    let descInfo = {'project_description':desc,'project_id':id};
    this.updateProject.updateDesc(descInfo).subscribe(data =>{
      console.log(data); 
      if(data.hasOwnProperty('status') && data['status'].toLowerCase() == 'success'){
        this.editModeDesc = false;
        this.store.dispatch(new updateDesc({'id':id,'desc':desc}))
      }     
  })
  }

    updateDuedate(date:any,id:any){    
    let descInfo = {'project_duedate':date,'project_id':id};
    console.log(descInfo);    
    this.updateProject.updateDuedate(descInfo).subscribe(data =>{      
      console.log(data); 
      if(data.hasOwnProperty('status') && data['status'].toLowerCase() == 'success'){
        this.editModeDueDate = false;
        this.store.dispatch(new updateDuedate({'id':id,'dueDate':date}))
      }     
    })
  }

  updateTitle(title:string,id:any){
    if(title.length !== 0){
      let titleInfo = {'project_title':title,'project_id':id,'user_id':this.loggedInUser}
      this.updateProject.updateTitle(titleInfo).subscribe(data =>{
      console.log(data); 
      if(data.hasOwnProperty('status') && data['status'].toLowerCase() == 'success'){
        this.editModeTitle = false;
        this.store.dispatch(new updateTitle({'id':id,'title':title}))
        }     
      })
    }        
  }

  deleteMember(memberId:any,projectId:any){
    let memberInfo = {'member_id':memberId,'project_id':projectId};
    this.updateProject.deleteMember
    this.updateProject.deleteMember(memberInfo).subscribe(data =>{
      console.log(data);
      if(data.hasOwnProperty('status') && data['status'].toLowerCase() == 'success'){
        // this.editModeTitle = false;
        this.store.dispatch(new deleteMember({'id':projectId,'member_id':memberId}))
        }  
    })

  }



  addMember(event:any,id:any,type:string,is_admin:any){
    alert(type);
    if(type == 'member_selection'){
          console.log(event);
          let bool = false;
          this.project['total_member'].forEach((e,i)=>{      
              console.log(e['member_id'] == event.userID)
              if(e['member_id'] == event.userID){
                bool = true;
                return;
              }
            })
          if(bool){
            alert('member already exists')
            return;
          }    
          this.updateProject.addMember(id,event.userID).subscribe(data =>{
            console.log(data);
          });
          this.store.dispatch(new addMember({'projectId':id,'id': event.userID, 'name':event.userName, user_image: ""}))
          }else if(type == 'admin_selection' && is_admin == 'true'){
            let bool = false;
            this.project['total_admin'].forEach((e,i)=>{ 
              if(e['admin_id'] == event.userID){
                bool = true;
                return;
              }
            })
            if(bool){
            alert('admin already exists')
            return;
            }
            let addAdminData = {'projectId':id,'userId':event.userID,'byUserId':this.loggedInUser}
            this.updateProject.addAdmin(addAdminData)
            .subscribe(data =>{
              console.log(data);
              if(data.hasOwnProperty('status') && data['status'].toLowerCase() == 'success'){
                this.store.dispatch(new addAdmin({'projectId':id,'id':event.userID,'name':event.userName}))
              }
            })
          }

  }

  deleteProject(id:any,is_admin:any){    
    if(is_admin == 'true'){
      this.deleteProjectservice.deleteProject(id).subscribe(data =>{
      console.log(data);
      if(data.hasOwnProperty('status') && data['status'].toLowerCase() == 'success'){
        this.store.dispatch(new deleteProject({'projectId':id}))
        this.store.dispatch(new paginate({'paginationNum':this.currPaginate,'paginationId':0}))
        this.getBack()
      }
    })
    }else{
      window.alert('You are not admin of this project');
    }
  }

  public reopenProject (id:any){
    this.updateProject.reOpenProject({'project_id':id})
        .subscribe(data =>{
         console.log(data);
         if(data['status'] == 'Success'){
           this.store.dispatch(new reOpenProject({'id':id}))           
           this.store.dispatch(new getAllProjects({'id':id}))           
           this.store.dispatch(new getProject({'id':this.projectId}));
           this.store.dispatch(new closedProjects({}))
           this.store.dispatch(new paginate({'paginationNum':1,'paginationId':0}))
         }
       })
  }

  public deleteAdmin(adminId:any,projectId:any,is_admin:any){

    if(is_admin == 'true'){
      let info = {'project_id':projectId,'member_id':adminId,'byUser':this.loggedInUser}
      this.updateProject.deleteAdmin(info)
      .subscribe(data =>{
        if(data.hasOwnProperty('status') && data['status'].toLowerCase() == 'success'){
          this.store.dispatch(new deleteAdmin({'id':projectId,'admin_id':adminId}))
        }
        console.log(data);
      })
    }
  }

}
