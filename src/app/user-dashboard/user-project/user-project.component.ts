import { Component, OnInit,OnChanges,ComponentRef } from '@angular/core';
import{ActivatedRoute,Router} from '@angular/router';
import {RouterParamsCatcherService} from '../../service/router-params-catcher.service';
import {SearchComponentComponent} from '../../search-component/search-component.component';
import {PaginationComponent} from '../../pagination/pagination.component';
import {Store} from'@ngrx/store';
import {AppState} from '../../state/app-state';
import {Observable} from 'rxjs';
import {GetUserDetailsService} from '../../service/get-user-details.service';
import {
  trigger,
  state,
  style,
  animate,
  transition ,
  query,
  group 
} from '@angular/animations';
import {sliceProjects,paginate,getAllProjects,closeProject} from '../../Action/action';
import {AddProjectInfoService} from '../../service/add-project-info.service';



@Component({
  selector: 'app-user-project',
  templateUrl: './user-project.component.html',
  styleUrls: ['./user-project.component.css']
})
export class UserProjectComponent implements OnInit,OnChanges{

  isOpen:boolean = false;
  isProjectView:boolean=true;
  loggedInUser:any;

  toggle(id){
    this.isProjectView = !this.isProjectView;
  }

userProject:any;
miniProjectArr:any = [];
projectArr:Array<any>;
public headerName:string = 'User Projects';
private project_admin:Array<any> = [];
itemPerpage:number = 5;
  state:Observable<AppState>;


  constructor(private getUser:GetUserDetailsService,private route:ActivatedRoute,private router:Router,private projectIdCatch:RouterParamsCatcherService,private store:Store<AppState>,private projectManipulate:AddProjectInfoService) {

      this.state = this.store.select('mainState');    
      this.store.dispatch(new getAllProjects({})); 

     //this.store.dispatch(new sliceProjects({'itemPerpage':this.itemPerpage}))
      this.store.dispatch(new paginate({'paginationNum':1,'paginationId':0}))
      this.state.subscribe((data) =>{             
      this.miniProjectArr = data.projects
      this.projectArr = data.paginateProjects; 
       this.loggedInUser = data.userId; 
      // console.log('OLA');
      console.log(data)    
     })

}



  ngOnInit() {   
    

  }

  ngOnChanges(){
    alert()
  }

   private openProject(id){
     this.projectIdCatch.set_id(id);           
     this.router.navigate(['/project/'+id+'']);   
   }

   nextPages(event){     
     // this.state.subscribe((data) =>{
     //  this.projectArr = data.paginateProjects;           
     // })
     // this.projectArr = []
     // console.log('DUPUR');
     
     // this.projectArr = event;
     // this.store.dispatch(new paginate({'paginationNum':2,'paginationId':0}))
     // console.log(this.projectArr);
   }

   showQuickView(id:string){
     
   }

   routeActivate(component:ComponentRef<any>){
    console.log(component);
      if(component.hasOwnProperty('miniProjectArr')){
        component['miniProjectArr'] = this.miniProjectArr;        
      }else if(component.hasOwnProperty('loggedInUser')){
        component['loggedInUser'] = this.loggedInUser;
      }
   }
   checkValue(event:any,id:any){
     if(event.currentTarget.checked){
       this.projectManipulate.closeProject({'project_id':id})
       .subscribe(data =>{
         console.log(data);
         if(data['status'] == 'Success'){
           this.store.dispatch(new closeProject({'id':id}))
         }
       })
     }     
   }

}
