import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-team',
  templateUrl: './user-team.component.html',
  styleUrls: ['./user-team.component.css']
})
export class UserTeamComponent implements OnInit {

  public headerName:string = 'User Team';

  constructor() { }

  ngOnInit() {
  }

}
