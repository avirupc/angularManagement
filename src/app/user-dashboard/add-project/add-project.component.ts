import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {AddProjectService} from '../../service/add-project.service';
import{UserDataProviderService} from '../../service/user-data-provider.service';
import {User} from '../../class/user';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import {NgForm} from '@angular/forms';
import {Store} from '@ngrx/store';
import {AppState} from '../../state/app-state';
import {Subscription} from 'rxjs';
import {addProject,paginate} from '../../Action/action';
import { trigger, transition, useAnimation } from '@angular/animations';
import { bounce,zoomIn } from 'ng-animate';
 

@Component({
  selector: 'app-add-project',
  templateUrl: './add-project.component.html',
  styleUrls: ['./add-project.component.css'],
  animations: [
    trigger('zoomIn', [transition('* => *', useAnimation(zoomIn,{
      params: { timing: .2 }
   }))])
  ], 
})
export class AddProjectComponent implements OnInit {

	project:Object;
	user:User;
  faTimes = faTimes;
  loggedInUser:any;  
  projectState:any;
  zoomIn:any;

  constructor(private router:Router,private projectAdd:AddProjectService,private userData:UserDataProviderService,private store:Store<AppState>) { }

  ngOnInit() {
    this.projectState = this.store.select('mainState'); 
    this.projectState.subscribe();
  }

  private addProject(info:NgForm){  
  	console.log(info.value)
    if(info.valid){
  	let adminId = parseInt(localStorage.getItem('id'));

  	if(adminId > 0){
  		let title = info.value.projectTitle;
	  	let desc = info.value.desc;
	  	let dueDate = info.value.dueDate;
	  	let isPublic = info.value.isPublic;	 
      let startDate = new Date();
      alert(startDate);
	  	this.project = {'adminId':adminId,'title':title,'desc':desc,'dueDate':dueDate,'startDate':startDate,'is_public':isPublic}

  	}	
  	console.log(this.project)
  	this.projectAdd.projectAddition(this.project)
  	.subscribe(
  		data=>{
  			console.log(data);

        if(data.hasOwnProperty('status')){          
          if(data['status'].toLowerCase() == 'success'){            
            if(this.userData.userData !== undefined){
              this.user = this.userData.userData; 
              this.store.dispatch(new addProject({'id':data[0]['project_id'],'title':info.value['projectTitle'],'description':info.value['desc']?info.value['desc']:"",'dueDate':info.value['dueDate'],'is_public':info.value['isPublic']}))
              this.store.dispatch(new paginate({'paginationNum':1,'paginationId':0}))
              let id = localStorage.getItem('id');
              this.router.navigate(['/dashboard/'+id+'/user-project']);              
            }else{              
              this.store.dispatch(new addProject({'id':data[0]['project_id'],'title':info.value['projectTitle'],'description':info.value['desc']?info.value['desc']:"",'dueDate':info.value['dueDate'],'is_public':info.value['isPublic']}))
              this.store.dispatch(new paginate({'paginationNum':1,'paginationId':0}))
              let id = localStorage.getItem('id');
              this.router.navigate(['/dashboard/'+id+'/user-project']);
            }
          }
        }
  		},
  		error=>{
  			console.log(error);
  		}
  		)
  }
  }

  close(){
    this.router.navigate(['/dashboard/'+this.loggedInUser+'/user-project']);
  }

}
