import { Component, OnInit } from '@angular/core';
import {Store} from '@ngrx/store';
import {AppState} from '../../state/app-state';
import {allAction,closedProjects,paginate,closeProject,reOpenProject,getTrashedProjects,reStoreProject,permanentDeleteProject} from '../../Action/action';
import {AddProjectInfoService} from '../../service/add-project-info.service';
import {Router} from '@angular/router';
import {DeleteProjectService} from '../../service/delete-project.service';
@Component({
  selector: 'app-trashed-project',
  templateUrl: './trashed-project.component.html',
  styleUrls: ['./trashed-project.component.css']
})
export class TrashedProjectComponent implements OnInit {

	public headerName:string = 'Closed Projects';
	private state:any;
	miniProjectArr:any = [];
	projectArr:Array<any>;
	currPaginate:any;

  constructor(private store:Store<AppState>,private editProject:DeleteProjectService) {
  	this.state  = this.store.select('mainState');
  	this.store.dispatch(new getTrashedProjects({}))
  	this.store.dispatch(new paginate({'paginationNum':1,'paginationId':0}))
  	this.state.subscribe((data) =>{ 
      this.projectArr = data.paginateProjects;
      this.currPaginate = data.currentPagination;
      console.log(this.projectArr)    
     }) 

  	   }

  ngOnInit() {
  }
  restore(id:any,is_admin:any){
  	alert(id)
  	if(is_admin == 'true'){
      this.editProject.restoreProject(id).subscribe(data =>{
      console.log(data);
      if(data.hasOwnProperty('status') && data['status'].toLowerCase() == 'success'){
        this.store.dispatch(new reStoreProject({'projectId':id}))
        this.store.dispatch(new paginate({'paginationNum':this.currPaginate,'paginationId':0}))        
      }
    })
    }else{
      window.alert('You are not admin of this project');
    }
  }

  permanentDelete(id:any,is_admin:any){

  	if(window.confirm('Do you really want to selete the Projects'))
  	{
  	if(is_admin == 'true'){
  		this.editProject.permanentDelete(id)
  		.subscribe(data =>{
      console.log(data);
      if(data.hasOwnProperty('status') && data['status'].toLowerCase() == 'success'){
        this.store.dispatch(new permanentDeleteProject({'projectId':id}))
        this.store.dispatch(new paginate({'paginationNum':this.currPaginate,'paginationId':0}))        
      }
    })
    }else{
      window.alert('You are not admin of this project');
    }
  			}
  	}
  }
