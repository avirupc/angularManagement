import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrashedProjectComponent } from './trashed-project.component';

describe('TrashedProjectComponent', () => {
  let component: TrashedProjectComponent;
  let fixture: ComponentFixture<TrashedProjectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrashedProjectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrashedProjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
