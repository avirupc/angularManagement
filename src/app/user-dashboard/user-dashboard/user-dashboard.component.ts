import { Component, OnInit,ViewChild,AfterViewInit,ComponentRef,ElementRef,Renderer2} from '@angular/core';
import {Router,ActivatedRoute} from '@angular/router';

import{UserDataProviderService} from '../../service/user-data-provider.service';
import {User} from '../../class/user';
import {GetUserDetailsService} from '../../service/get-user-details.service';
import {MiniProjects } from '../../class/mini-projects';
import {RouterParamsCatcherService} from '../../service/router-params-catcher.service';
import {DashboardComponent} from '../dashboard/dashboard.component';
import {SharedLogoutService} from '../../service/shared-logout.service';
import {DataObserverService} from '../../service/data-observer.service';
import {Observable,BehaviorSubject} from 'rxjs';
import {Store} from '@ngrx/store';
import {AppState} from '../../state/app-state';
import {allAction,getUserDetails,closedProjects,paginate,getAllProjects} from '../../Action/action';
 


@Component({
  selector: 'app-user-dashboard',
  templateUrl: './user-dashboard.component.html',
  styleUrls: ['./user-dashboard.component.css']
})
export class UserDashboardComponent implements OnInit,AfterViewInit  {

  @ViewChild( 'view') component:ComponentRef<DashboardComponent>;
  @ViewChild('userDashboard') userDashboard:ElementRef;
  @ViewChild('dashboardInfo') dashboardInfo:ElementRef;
  @ViewChild('workspaceMenu') workspaceMenu:ElementRef;

  private user:User;
  private miniProjectinfo :MiniProjects;
  private miniProjectArr :MiniProjects[] = [];
  private workspace:Array<any> = [];
  private userID:any;
  public userDetails:any;
  public task_count:any;
  public projectCount:any;
  private child_route_name:string;
  private itemsPerpage:number = 5;
  private state;
  loggedInUser:any;
  data:Array<any>=[];
  private showMenu:boolean=true;
  private menuId:any


	 
  constructor(private render:Renderer2,private store:Store<AppState>,private userData:UserDataProviderService,private getUser:GetUserDetailsService,private router:Router,private projectIdCatch:RouterParamsCatcherService,private activateRoute:ActivatedRoute,private logout:SharedLogoutService,private dataObserver:DataObserverService) {
    this.userDetails = this.activateRoute.snapshot.data['userDetails'];
    this.task_count = this.userDetails.task_count;    

    this.state = store.select('mainState');
    store.dispatch(new getUserDetails(this.userDetails))
    this.state.subscribe(data =>{
      console.log("*********************************************")
      console.log(data)
      this.user = data.user;
      this.projectCount = data.projects.length;
      this.workspace = data.activeWorkspace;      
      console.log("*********************************************")
      console.log(data.workspace)
    })
  		}  

   ngAfterViewInit(){  
       this.render.addClass(this.workspaceMenu.nativeElement,'active')

     // console.log(this.component);
     let windowHeight = window.innerHeight;
     console.log(windowHeight);
     this.userDashboard.nativeElement.style.height = windowHeight+'px';
     this.dashboardInfo.nativeElement.style.height = windowHeight+'px';
     let dashboard = this.userDashboard.nativeElement;
     let dashboardInfo = this.dashboardInfo.nativeElement;
     window.addEventListener('resize',function(){
       let windowHeight = window.innerHeight;
       dashboard.style.height = windowHeight+'px';
       dashboardInfo.style.height = windowHeight+'px';
     })  

   }

   private openProject(id){
     this.projectIdCatch.set_id(id);           
     this.router.navigate(['/project/'+id+'']);   
   }	
   private activeRoute(component){     
     component.user = this.user;
     component['loggedInUser'] = this.loggedInUser;        
     this.child_route_name = component.headerName;
   }

   onChabgeFile(event){

   }

   Logout(){
     localStorage.removeItem('isLogin');
     localStorage.removeItem('id');
     localStorage.removeItem('email');
     this.router.navigate(['login']);
     this.logout.logoutCommand({'isLogin': false,userId:''});
   }

   uploadImg(){     
     
   }

    ngOnInit() {             
          }
   closedProjects(){
     this.store.dispatch(new closedProjects({'is_complete':"1"}))
      this.store.dispatch(new paginate({'paginationNum':1,'paginationId':0}))
          }
    getAlProjects(){ 
            this.store.dispatch(new getAllProjects({})); 
            this.store.dispatch(new paginate({'paginationNum':1,'paginationId':0}))            
          }

      openMenu(wsId:any){
        if(this.menuId == wsId){
          this.showMenu = !this.showMenu;
        }else{
          this.showMenu = true;
          this.menuId = wsId;
        }            
          
          }
  }


