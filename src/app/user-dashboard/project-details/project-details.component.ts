import { Component, OnInit,ComponentRef} from '@angular/core';
import {Router,ActivatedRoute} from '@angular/router';
import {Observable,Subscription} from 'rxjs';
import {Store} from '@ngrx/store';
import {AppState} from '../../state/app-state';
import {GetProjectHistoryService} from '../../service/get-project-history.service';


@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['./project-details.component.css']
})
export class ProjectDetailsComponent implements OnInit {

	private projectState:any;
	public headerName:string = 'User Projects Details';
	private project:any;
  private projects_status:string='';
  private history:Array<any>;
  private creator_name:string = ''; 
  public projectId:any;

  constructor(private router:Router,private activateRoute:ActivatedRoute,private store:Store<AppState>,private getHistory:GetProjectHistoryService) {

    this.projectState = store.select('mainState');
  	activateRoute.params.subscribe(data =>{
  		console.log(data);
  		if(data.hasOwnProperty('id')){     
      this.projectId = data['id'];   
        getHistory.getHistory({'project_id':data['id']}).subscribe(history =>{
          console.log(history);
          if(history.hasOwnProperty('status') && history['status'].toLowerCase() == 'success'){
            this.history = history['history'];
          }          
        })
  			this.projectState.subscribe(projects =>{
  				projects.projects.forEach((e,i)=>{
  					if(e.id == data ['id']){
  						this.project = e;
              this.creator_name = e['creator_info'][0]['creator_name'];
              if(e['is_complete'] == 0){
                this.projects_status = 'Open';
              }else if(e['is_complete'] == 1){
                this.projects_status = 'Closed';
              }
  					}	
  				})
  				console.log(this.project)
  			})
  		}
  	})


   }

  ngOnInit() {
  }
  routerActivate(component:ComponentRef<any>){    
    component['projectId'] = this.projectId;
  }

}
