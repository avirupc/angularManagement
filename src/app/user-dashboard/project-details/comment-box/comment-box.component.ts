import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute} from '@angular/router';
import {AddCommentService} from '../../../service/add-comment.service';
import {Store} from '@ngrx/store';
import {AppState} from '../../../state/app-state';
import {allAction ,getComment,addComment} from '../../../Action/action';
import {UploaderComponent} from '../../uploader/uploader.component';

@Component({
  selector: 'app-comment-box',
  templateUrl: './comment-box.component.html',
  styleUrls: ['./comment-box.component.css']
})
export class CommentBoxComponent implements OnInit {


  private projectState:any;	
  private project:any;
  private projects_status:string='';
  private history:Array<any>;
  private creator_name:string = ''; 
  public projectId:any;
  public comment:Array<any> =[];
  public data;
  private LoggedInUser:any;
  private getComment:Array<any>;
  private stateComment:any;

  constructor(private store:Store<AppState>,private activateRoute:ActivatedRoute,private addComment :AddCommentService ) { }

  ngOnInit() {

  	   	this.projectState = this.store.select('mainState');
  	this.activateRoute.parent.params.subscribe(data =>{  		
  		if(data.hasOwnProperty('id')){ 

        //LIST COMMENT

        this.addComment.getComment({'project_id':data['id']})
        .subscribe(comment =>{
          console.log(comment);
          this.stateComment = [];
          if(comment.hasOwnProperty('status') && comment['status'].toLowerCase() == 'success'){          	
            this.store.dispatch(new getComment({'comment': comment['comment']}))             
          }          
        })

  			this.projectState.subscribe(projects =>{  			
  				this.LoggedInUser = projects.userId;
  				this.stateComment = projects.comment;
  				projects.projects.forEach((e,i)=>{
  					if(e.id == data ['id']){
  						this.project = e;

  						
  						console.log(e);
              this.creator_name = e['creator_info'][0]['creator_name'];
              if(e['is_complete'] == 0){
                this.projects_status = 'Open';
              }else if(e['is_complete'] == 1){
                this.projects_status = 'Closed';
              }
  					}	
  				})
  				console.log(this.project)
  			})
  		}
  	})
  }


  pushComment(event:any){
  this.comment = [];
  this.comment.push(event.html);
  }

    DoComment(project_id:any){
  	console.log(this.comment[0])
  	if(this.data.length > 0){
  		let info = {'project_id':project_id,'comment':this.comment[0],'user_id':this.LoggedInUser}
  	this.addComment.addComment(info)
  	.subscribe(data =>{

  		if(data.hasOwnProperty('status') && data['status'].toLowerCase() == 'success'){
            // alert('Success');
            this.data = '';
            this.store.dispatch(new addComment({'id':data['last_comment_id'],'user_id':this.LoggedInUser,'user_name':data['login_name'],'user_image':data['user_image'],'post_date':data['last_post_date'],'desc':this.comment[0]}))
          }  
  	})
  } 	
  	
  }

}
