import { Component, OnInit } from '@angular/core';
import {Store} from '@ngrx/store';
import {AppState} from '../../state/app-state';
import {allAction,closedProjects,paginate,closeProject,reOpenProject} from '../../Action/action';
import {AddProjectInfoService} from '../../service/add-project-info.service';
import {Router} from '@angular/router';
@Component({
  selector: 'app-closed-projects',
  templateUrl: './closed-projects.component.html',
  styleUrls: ['./closed-projects.component.css']
})
export class ClosedProjectsComponent implements OnInit {

	public headerName:string = 'Closed Projects';
	private state:any;
	miniProjectArr:any = [];
	projectArr:Array<any>;

  constructor(private store:Store<AppState>,private projectManipulate:AddProjectInfoService,private router:Router) {
  	
   }

  ngOnInit() {
  	this.state = this.store.select('mainState');
  	this.store.dispatch(new closedProjects({}))
  	this.store.dispatch(new paginate({'paginationNum':1,'paginationId':0}))
  	this.state.subscribe((data) =>{             
      // this.miniProjectArr = data.closeProjects;       
      this.projectArr = data.paginateProjects; 
      // console.log('OLA');
      console.log(this.projectArr)    
     })  	
  	
  }

}
