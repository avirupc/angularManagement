import { Component, OnInit,Input,AfterViewInit} from '@angular/core';
import {User} from '../../class/user';
import{ActivatedRoute} from '@angular/router';
import {RouterParamsCatcherService} from '../../service/router-params-catcher.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit,AfterViewInit {

	user:any;
	userDetails:any;	
  public headerName:string = 'User Dashboard';
  imageUrl:string = '../assets/img/wallpaper/city.jpg';
  user_member_since:string;  

  private dateArray:Array<string> = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

  constructor(private userCatcher:RouterParamsCatcherService,private route:ActivatedRoute) {   

  		// GET DATA FROM RESOLVE
  		   // this.userDetails = this.route.snapshot.data['userDetails']; 
       //   this.user = this.userDetails.users[0] 
         
       //    if(this.user['user_registration_date'].length > 0){

       //      let member_since = this.user['user_registration_date'].split(' ');
       //       console.log(member_since)
       //      let dateOnly = member_since[0].split("-");
       //      let year = dateOnly[0];
       //      this.user_member_since = dateOnly[2]+" "+this.dateArray[dateOnly[1]-1]+" "+year;
       //    } 

         // this. 
         // this.userCatcher.get_user().subscribe(data =>{
         // 	this.userDetails = data;
         // 	this.user = this.userDetails.users
         // })
         // this.user = this.userDetails.users
         // console.clear()
         console.log(this.user);
        // GET DATA FROM ROUTER PARAMS 

  	  	 // this.route.queryParams.subscribe(data =>{
  	  	 	
  	  	 // 	if(data.hasOwnProperty('loginName')){
  	  	 // 		this.user = data 
  	  	 // 	}else{
  	  	 // 		this.userDetails.user = this.user
  	  	 // 	}
  	  	 // 	// console.log(data)
  	  	 // 	// console.log(this.userDetails);
  	  	 	
  	  	 // })

  }
  ngAfterViewInit(){  	
  	console.log(this.route.snapshot.data['user'])
	
  }

  ngOnInit() {  
         
   }

}
