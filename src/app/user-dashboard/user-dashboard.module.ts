import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BrowserModule} from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import{FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';
import {SearchComponentComponent} from '../search-component/search-component.component';
import {PaginationComponent} from '../pagination/pagination.component';
import {SearchbarComponent} from '../common-view/searchbar/searchbar.component';
import {CommonViewModule} from '../common-view/common-view.module';
import {HttpModule} from '@angular/http';
import { QuillModule } from 'ngx-quill';
import { FileSelectDirective } from 'ng2-file-upload';
// Add an icon to the library for convenient access in other components
library.add(fas, far);

// IMPORT ROUTER MODULE
import {AppRouterModule} from '../app-router/app-router.module';

import {AddProjectService} from '../service/add-project.service';

// COMPONENT
import { IndexComponent } from '../index/index.component';
import { UserDashboardComponent } from './user-dashboard/user-dashboard.component';
import { AddProjectComponent } from './add-project/add-project.component';
import { UserProjectComponent } from './user-project/user-project.component';
import { UserTaskComponent } from './user-task/user-task.component';
import { UserTeamComponent } from './user-team/user-team.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProjectQuickViewComponent } from './user-project/project-quick-view/project-quick-view.component';
import { ProjectDetailsComponent } from './project-details/project-details.component';
import { ProjectInfoComponent } from './project-details/project-info/project-info.component';
import { TaskBoardComponent } from './project-details/task-board/task-board.component';
import { ClosedProjectsComponent } from './closed-projects/closed-projects.component';
import { OpenProjectsComponent } from './open-projects/open-projects.component';
import { TrashedProjectComponent } from './trashed-project/trashed-project.component';
import { UploaderComponent } from './uploader/uploader.component';
import { CommentBoxComponent } from './project-details/comment-box/comment-box.component';
import { SearchResultComponent } from './search-result/search-result.component';
import{CloseOpenToggleComponent} from '../close-open-toggle/close-open-toggle.component';
import { SlimScroll } from 'angular-io-slimscroll';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    AppRouterModule,
    FormsModule,
    FontAwesomeModule,
    CommonViewModule,
    HttpModule,
    QuillModule,
    NgbModule    
  ],
  declarations: [FileSelectDirective,UserDashboardComponent, AddProjectComponent, UserProjectComponent, UserTaskComponent, UserTeamComponent, DashboardComponent,SearchComponentComponent,PaginationComponent, ProjectQuickViewComponent, ProjectDetailsComponent, ProjectInfoComponent, TaskBoardComponent, ClosedProjectsComponent, OpenProjectsComponent, TrashedProjectComponent, UploaderComponent, CommentBoxComponent, SearchResultComponent,CloseOpenToggleComponent,SlimScroll],
  bootstrap:[IndexComponent],  
  providers:[AddProjectService],
  exports:[FileSelectDirective,CloseOpenToggleComponent]
})
export class UserDashboardModule { }
