import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {Store} from '@ngrx/store';
import {Router} from '@angular/router';
import {AppState} from '../state/app-state';
import {addProject,paginate,modalClose,addWorkspace} from '../Action/action';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import {WorkspaceService} from '../service/workspace.service';
import {AddProjectService} from '../service/add-project.service';
@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {

	private state:any;
	private modalOpen:boolean=false;
	private faTimes = faTimes;
	private showModalContent:string='';
  private loggedInUser:any;
  private project:any;

  constructor(private router:Router,private store:Store<AppState>,private workspaceSevice:WorkspaceService,private projectAdd:AddProjectService) { }

  ngOnInit() {
  	this.state = this.store.select('mainState');
  	this.state.subscribe((state)=>{
  		this.modalOpen = state.modalOpen;
  		this.showModalContent = state.showModalContent;
      this.loggedInUser  = state.userId;
  	})
  }
  close(){
  	this.store.dispatch(new modalClose({}))
  }
  addWorkSpace(f:NgForm){
    console.log('fffrrr')
    console.log(f.value.ws_title);
    f.value['creator_id'] =this.loggedInUser;
    if(f.valid){
      this.workspaceSevice.addWorkspace(f.value)
      .subscribe(data =>{
        console.log(data);
        if(data.hasOwnProperty('status') && data['status'].toLowerCase() == 'success'){
          this.store.dispatch(new addWorkspace({'ws_id':data['last_ws_id'],'title':f.value.ws_title,'desc':f.value['ws_desc']}));
          this.store.dispatch(new modalClose({}))
        }
      })
    }
  }


    private addProject(info:NgForm){  
    console.log(info.value)
    if(info.valid){
    let adminId = parseInt(localStorage.getItem('id'));

    if(adminId > 0){
      let title = info.value.projectTitle;
      let desc = info.value.desc;
      let dueDate = info.value.dueDate;
      let isPublic = info.value.isPublic;   
      let startDate = new Date();
      alert(startDate);
      this.project = {'adminId':adminId,'title':title,'desc':desc,'dueDate':dueDate,'startDate':startDate,'is_public':isPublic}

    }  
    console.log(this.project)
    this.projectAdd.projectAddition(this.project)
    .subscribe(
      data=>{
        console.log(data);

        if(data.hasOwnProperty('status')){          
          if(data['status'].toLowerCase() == 'success'){            
            if(this.loggedInUser !== undefined){               
              this.store.dispatch(new addProject({'id':data[0]['project_id'],'title':info.value['projectTitle'],'description':info.value['desc']?info.value['desc']:"",'dueDate':info.value['dueDate'],'is_public':info.value['isPublic']}))
              this.store.dispatch(new paginate({'paginationNum':1,'paginationId':0}))
              this.router.navigate(['/dashboard/'+this.loggedInUser+'/user-project']);              
            }else{              
              this.store.dispatch(new addProject({'id':data[0]['project_id'],'title':info.value['projectTitle'],'description':info.value['desc']?info.value['desc']:"",'dueDate':info.value['dueDate'],'is_public':info.value['isPublic']}))
              this.store.dispatch(new paginate({'paginationNum':1,'paginationId':0}))
              let id = localStorage.getItem('id');
              this.router.navigate(['/dashboard/'+id+'/user-project']);
            }
          }
        }
      },
      error=>{
        console.log(error);
      }
      )
  }
  }

}
