import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';

// IMPORT REGISTER SERVICE
import {RegisterService} from '../service/register.service';
import {Observable,Subscription} from 'rxjs';

// IMPORT ROUTER
import{Router} from '@angular/router';


@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  postData:Subscription;
  isLogin:any;
  userId:any;
  	
  constructor(private register:RegisterService,private router:Router) {

   this.isLogin = localStorage.getItem('isLogin');
   this.userId =  localStorage.getItem('id');

   if(this.isLogin == 'true'){

     if(this.userId > 0){
      this.router.navigate(['dashboard/'+this.userId+'']);
    }
    }
   }

  ngOnInit() {
  }

  registration(userInfo:NgForm){
  	console.log(userInfo.valid);

    let toDay = new Date();

    if(userInfo.valid){
          userInfo.value['registration_date'] = toDay;
          this.postData = this.register.register(userInfo.value)
    .subscribe(
      data=>{

         if(data.hasOwnProperty('status')){

           if(data['status'] == 'Success'){             
             console.log(this.router)
             this.router.navigate(['login']);
           }else if(data['status'] !== 'Success'){
             alert(data['status']);
           }
         }
      },
      error=>{
        console.log(error)
      }

      )
    }

  }

}
