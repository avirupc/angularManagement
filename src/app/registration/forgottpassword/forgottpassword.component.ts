import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {LoginService} from '../service/login.service';

@Component({
  selector: 'app-forgottpassword',
  templateUrl: './forgottpassword.component.html',
  styleUrls: ['./forgottpassword.component.css']
})
export class ForgottpasswordComponent implements OnInit {

  constructor(private login:LoginService) { }


  private message:string;
  private emailFound:boolean;
  private showMessage :boolean;
  ngOnInit() {
  	this.showMessage = false;
  }

  onSubmit(f:NgForm){
  	console.log(f.value);
  	this.login.forgotPassword(f.value).subscribe(data =>{
  		if(data.hasOwnProperty('status')){

  			this.showMessage = true;  			
  			if(data['status'].toLowerCase() == 'success'){
  				this.message = data['message'];
  				this.emailFound = true;
  			}else if(data['status'].toLowerCase() == 'failure'){
  				this.message = data['message'];
  				this.emailFound = false;
  			}
  		}
  	})
  }

}
