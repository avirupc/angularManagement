import { Component, OnInit,AfterViewInit,Output,EventEmitter} from '@angular/core';
import {LoginService} from '../../service/login.service';
import {
   AuthService,   
   GoogleLoginProvider
} from 'angular-6-social-login';
declare let gapi:any;
@Component({
  selector: 'app-googlelogin',
  templateUrl: './googlelogin.component.html',
  styleUrls: ['./googlelogin.component.css']
})


export class GoogleloginComponent implements OnInit,AfterViewInit {
	 
	 auth:any;
	 user:Array<any> = [];
	 @Output() userLogin = new EventEmitter();
	 @Output() message:any

  constructor(private loginService:LoginService,private authService:AuthService) { }

  ngOnInit() {
  }

  socialSignin(){
  	this.authService.signIn(GoogleLoginProvider.PROVIDER_ID).then(data=>{
  		console.log(data);
  		if(data !== null)
  		this.apiCall(data)
  	})
  }


  apiCall(data){
          
        this.loginService.socialLogin(data).subscribe(data=>{
        	console.log(data);
        	if(data.hasOwnProperty('status') && data['status'].toLowerCase() == 'success'){
        		this.userLogin.emit(data);
        		this.message = data['message']
        	}else{
        		this.message = data['message']
        	}
        })
  }


  ngAfterViewInit(){

       }




}
