import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule  }   from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import{AppRouterModule} from '../app-router/app-router.module';

import { RegistrationComponent } from './registration/registration.component';
import { LoginComponent } from './login/login.component';

// IMPORT REGISTER SERVICE TO PROVIDE REGISTER SERVICE THROUGH OUT REGISTRATION MODULE
import{RegisterService} from './service/register.service';
import { ForgottpasswordComponent } from './forgottpassword/forgottpassword.component';
import { GoogleloginComponent } from './socialLogin/googlelogin/googlelogin.component';
declare var  gapi:any;

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRouterModule
  ],
  providers:[RegisterService],
  exports:[RegistrationComponent,LoginComponent,GoogleloginComponent],
  declarations: [RegistrationComponent,LoginComponent, ForgottpasswordComponent, GoogleloginComponent],
})
export class RegistrationModule { }
