import { Component, OnInit,Input } from '@angular/core';
import {Router} from '@angular/router';
import {LoginService} from '../service/login.service';
import {AppState} from '../../state/app-state';
import {Observable} from 'rxjs';
import {Store} from'@ngrx/store';
import {allAction,Login} from '../../Action/action';
import { NgForm } from '@angular/forms';

// IMPORT USER CLASS
import {User} from '../../class/user';

// IMPORT USER DATA PROVIDERS
import{UserDataProviderService} from '../../service/user-data-provider.service';
declare const gapi:any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @Input() isHome:any;

	user:User;
  isLogin:any;
  userId:any;
  public ishome:boolean;

  state:Observable<AppState>;

  constructor(private router:Router,private loginService:LoginService,private userData:UserDataProviderService,private store:Store<AppState>) {
     // this.isLogin = localStorage.getItem('isLogin');
    this.userId =  localStorage.getItem('id') ;

    this.state = store.select('mainState');
    this.state.subscribe(data =>{
      this.isLogin = data.isLogin;      
    })
   
     if(this.isLogin){
        alert(this.userId)
       if(parseInt(this.userId) > 0){
        this.router.navigate(['dashboard/'+this.userId+'']);
      }
      }
     }


     getLogin(userInfo:NgForm){
     if(userInfo.valid){
              this.loginService.login(userInfo.value)
       .subscribe(
         data =>{
           this.getUserInfo(data);
         },
         error =>{           
           console.log(error);
         }

         )  
         return false ;
     }
   	
     }

     public getUserInfo(data){
       alert()
         console.log(data)

         let id = data.hasOwnProperty('id')?data['id']:'';  
         let username = data.hasOwnProperty('userName')?data['userName']:'';        
         let userpass = data.hasOwnProperty('userPassword')?data['userPassword']:'';
         let useremail = data.hasOwnProperty('userEmail')?data['userEmail']:'';
         let usernicename = data.hasOwnProperty('userNiceName')?data['userNiceName']:'';

         if(data.hasOwnProperty('status')){

           if(data['status'] == 'Success'){

             this.store.dispatch(new Login({isLogin:true,userId:id}))

             this.user = new User(id,username,userpass,usernicename,useremail);
             this.userData.userData = this.user;
             console.log(this.userData.userData)
             localStorage.setItem('isLogin','true');
             localStorage.setItem('id',id);
             localStorage.setItem('email',useremail);
             this.router.navigate(['dashboard/'+id+'']);



           }else if(data['status'] !== 'Success'){
             alert(data['status']);
           }
         }
     }

  ngOnInit() {
  }

  onSignIn(googleUser) {
        // Useful data for your client-side scripts:
        var profile = googleUser.getBasicProfile();
        console.log("ID: " + profile.getId()); // Don't send this directly to your server!
        console.log('Full Name: ' + profile.getName());
        console.log('Given Name: ' + profile.getGivenName());
        console.log('Family Name: ' + profile.getFamilyName());
        console.log("Image URL: " + profile.getImageUrl());
        console.log("Email: " + profile.getEmail());

        // The ID token you need to pass to your backend:
        var id_token = googleUser.getAuthResponse().id_token;
        console.log("ID Token: " + id_token);
      };

}
