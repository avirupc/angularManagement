import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class LoginService {

	url:string = 'http://quicktask.in/avirup/admin/user/login';	
	forgotUrl:string = 'http://quicktask.in/avirup/admin/user/forgottpassword';
	socialLoginUrl:string = 'http://quicktask.in/avirup/admin/user/socialLogin';
  constructor(private http:HttpClient) {

   }

   login(userInfo){
   	return this.http.post(this.url,userInfo)
   }

   forgotPassword(email){
   	return this.http.post(this.forgotUrl,email)
   }

   socialLogin(userInfo){
   	console.log('///////////////////////')
   	console.log(userInfo);
   	return this.http.post(this.socialLoginUrl,userInfo);
   }
}
