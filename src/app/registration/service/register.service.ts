import { Injectable } from '@angular/core';
import {HttpClient,HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  url:string = 'http://quicktask.in/avirup/admin/user/addUser';	
  constructor(private http:HttpClient) {

   }

   register(userInfo){

   	return this.http.post(this.url,userInfo)

   }
}
