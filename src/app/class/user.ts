export class User {

 id:number;
 loginName:string;
 loginPass:string;
 userNiceName:string;
 userEmail:string;
 userImg:string;	

constructor(id:number,loginNname:string,loginPass:string,niceName:string,email:string,userImg?:string){
	this.id = id;  
	this.loginName = loginNname;
	this.loginPass = loginPass;
	this.userNiceName = niceName;
	this.userEmail = email;
	this.userImg = userImg
}
	
}
