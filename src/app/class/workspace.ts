export class Workspace {

id:number;
title:string;
desc:string;
startDate:Date;
creatorId:number;
status:string;
member:any[];
admin:any[];
team:any[];


constructor(title:string,desc:string,startDate:Date,creatorId:number,status:string,member:any[],admin:any[],team:any[],id?:number){
	this.id = id;
	this.title = title;
	this.desc=desc;	
	this.startDate = startDate;	
	this.creatorId = creatorId;	
	this.status = status;
	this.member = member;
	this.admin = admin;
	this.team = team;
	
}
}
