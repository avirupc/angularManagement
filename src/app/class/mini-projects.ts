export class MiniProjects implements MiniProjects {
	id:any;
	title:string;
	desc:string;
	is_creator:boolean;
	is_admin:boolean;
	dueDate:Date;
	startDate:Date;
	constructor(id:any,title:string,description:string,is_creator:boolean,is_admin:boolean,duedate:Date,startDate:Date){
		this.id = id;
		this.title = title;
		this.desc = description;
		this.is_creator = is_creator;
		this.is_admin = is_admin;
		this.dueDate = duedate;
		this.startDate = startDate;
	}
}
