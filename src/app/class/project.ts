export class Project {

id:number;
title:string;
desc:string;
dueDate:Date;
startDate:Date;
creatorId:number;
is_public:boolean;
status:string;
member:any[];
admin:any[];
team:any[];
task:any[];

constructor(title:string,desc:string,dueDate:Date,startDate:Date,creatorId:number,is_public:boolean,status:string,member:any[],admin:any[],team:any[],task:any[],id?:number){
	this.id = id;
	this.title = title;
	this.desc=desc;
	this.dueDate = dueDate;
	this.startDate = startDate;
	this.is_public = is_public;
	this.creatorId = creatorId;	
	this.status = status;
	this.member = member;
	this.admin = admin;
	this.team = team;
	this.task = task;
}


}
