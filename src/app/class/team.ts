import {User} from './user';
export class Team {

	id:any;
	members:User[];

	constructor(id:any,members:User[]){
		this.id = id;
		this.members = members
	}
}
