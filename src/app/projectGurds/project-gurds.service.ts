import { Injectable } from '@angular/core';
import {Router,CanLoad,CanActivate,ActivatedRoute,NavigationEnd} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import 'rxjs/add/operator/map';
import {RouterParamsCatcherService} from '../service/router-params-catcher.service';

@Injectable({
  providedIn: 'root'
})
export class ProjectGurdsService implements CanActivate {
    private urlParams:string;
	private url:string[];
	private params:number;
	private projectId:number;
    private queryIndex:any;
    private sub:any;
	private projectUrl:string = 'http://quicktask.in/avirup/admin/project/publicProject';
    private isLogin:any = 'false';
    private userId:any = '';

  constructor(private router:Router,private http:HttpClient,private snap:ActivatedRoute,private projectIdCatch:RouterParamsCatcherService) {
  	 let isLogin = localStorage.getItem('isLogin');
     let userId = localStorage.getItem('id');

       if(isLogin && isLogin == 'true'){
           if(userId && parseInt(userId) > 0){
               this.isLogin = 'true';
               this.userId = userId;
           }
       }

      }

      

  private isProject(){

           this.urlParams = window.location.href.replace(window.location.origin,'');
           this.url = this.urlParams.split('/');  
           console.log(this.url); 
       
           if(this.url[1] == 'project' && this.url[2].length > 0){                   
               if(parseInt(this.url[2]) > 0){
                 this.projectIdCatch.set_id(parseInt(this.url[2]))
                }
            }    

            console.log(this.projectIdCatch);  

        this.projectId = this.projectIdCatch.get_id();
        console.log(this.projectId); 

        // CHECKING PROJECT IS PUBLIC OR NOT
           
        return this.http.post(this.projectUrl,{'projectId':this.projectId,'userId':this.userId,'isLogIn':this.isLogin})
        .map(
                data=>{
                  console.log(data);

                  if(data.hasOwnProperty('status')){

                    if(data['status']){                     
                      return true;
                    }else{
                      this.router.navigate(['home'])
                      return false;
                    }
                   }
                },
                error =>{
                  console.log(error);
                  return false;
                }
          ) 
        }

  public  canActivate():Observable<boolean> | boolean{    

      return this.isProject();
          
    }   
       
}
