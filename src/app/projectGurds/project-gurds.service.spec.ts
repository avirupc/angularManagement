import { TestBed, inject } from '@angular/core/testing';

import { ProjectGurdsService } from './project-gurds.service';

describe('ProjectGurdsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProjectGurdsService]
    });
  });

  it('should be created', inject([ProjectGurdsService], (service: ProjectGurdsService) => {
    expect(service).toBeTruthy();
  }));
});
